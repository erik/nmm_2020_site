import sympy as sp

# Verifying that the Nelder-Mead reflection scheme in Numerical Methods is equivalent to the
# standard presentation.

x = sp.symbols("x", real=True)
x_1, x_2, x_3 = sp.symbols("x_1 x_2 x_3", real=True)
y_1, y_2, y_3 = sp.symbols("y_1 y_2 y_3", real=True)

term_1 = y_1 * (x - x_2) * (x - x_3) / (x_1 - x_2) / (x_1 - x_3)
term_2 = y_2 * (x - x_1) * (x - x_3) / (x_2 - x_1) / (x_2 - x_3)
term_3 = y_3 * (x - x_1) * (x - x_2) / (x_3 - x_1) / (x_3 - x_2)
poly = term_1 + term_2 + term_3
dpoly = sp.diff(poly, x)
sol = sp.solve(dpoly, x)
assert(len(sol) == 1)
sol = sol[0]

print("lagrange polynomial")
print(sp.latex(poly))
print("derivative")
print(sp.latex(dpoly))
print("minimum")
print(sp.latex(sol))

q = (x_2 - x_3) * (y_2 - y_1)
r = (x_2 - x_1) * (y_2 - y_3)
alt = x_2 - ((x_2 - x_3) * q - (x_2 - x_1) * r) / 2 / (q - r)
print(sp.latex(alt))

print(sp.simplify(sol - alt))
