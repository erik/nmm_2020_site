#ifndef INTEGRATORS_H
#define INTEGRATORS_H

#include <cmath>
#include <utility>

//--------------------------------------------------------------------------------------------------
// All these integrators are for numerically solving problems of the form  x'(t) = f(t, x). Note
// that x can be vector valued, so higher order problems can be solved by converting to a system of
// first order problems.

//--------------------------------------------------------------------------------------------------
// A single step of Euler integration
// This is commented out since I need to make its interface compatible with diff_euler_step.
/*
template <typename F, typename Vector, typename Scalar>
void euler_step(F const& f, Scalar const delta_t, Scalar& t, Vector& x) {
    x += delta_t * f(t, x);
    t += delta_t;
}
*/

//--------------------------------------------------------------------------------------------------
// A single step of differentiable Euler integration
template <typename F, typename Vector, typename Scalar>
void diff_euler_step(
    F& f,
    Scalar const delta_t,
    Scalar& t,
    Vector& state,
    MatrixX<Scalar>& state_jacobian
) {
    uint32_t const rows = state_jacobian.rows();
    uint32_t const cols = state_jacobian.cols();

    f(t, state);
    t += delta_t;
    state += delta_t * f.tangent();
    state_jacobian =
        (MatrixX<Scalar>::Identity(rows, cols) + delta_t * f.tangent_jacobian()) * state_jacobian;
}

#endif
