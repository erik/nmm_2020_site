#include "n_body.h"

//..................................................................................................
void NBodyLaw::resize(uint32_t n) {
    n_ = n;
    m_.resize(n_);
}

//..................................................................................................
void NBodyLaw::set_masses(VectorX<Scalar> const& m) {
    n_ = m.size();
    m_ = m;
}

//..................................................................................................
void NBodyLaw::operator()(VectorX<Scalar> const& state, VectorX<Scalar>& tangent) {
    // The derivatives of locations with respect to time are just the velocities, which we know.
    tangent.segment(0, n_ * d) = state.segment(n_ * d, n_ * d);

    // The derivatives of velocities with respect to time are accelerations. Here we use Newton's
    // law of universal gravitation to compute forces, then divide out the relevant mass to get
    // accelerations (or more literally, we never multiply by it in the first place). This involves
    // a sum over all pairs of particles, which could be truncated for distant particles if needed.
    tangent.segment(n_ * d, n_ * d).setZero();
    for (uint32_t i = 0; i < n_; ++i) {
        for (uint32_t j = i + 1; j < n_; ++j) {
            Eigen::Matrix<Scalar, d, 1> force = state.segment<d>(j * d) - state.segment<d>(i * d);
            Scalar const distance_squared = force.squaredNorm();
            Scalar const distance = std::sqrt(distance_squared);
            force *= G / (distance_squared * distance);
            tangent.segment<d>((n_ + i) * d) += m_[j] * force;
            tangent.segment<d>((n_ + j) * d) -= m_[i] * force;
        }
    }
}

//..................................................................................................
void NBodyLaw::operator()(
    VectorX<Scalar> const& state,
    VectorX<Scalar>& tangent,
    MatrixX<Scalar>& tangent_jacobian
) {
    // Compute derivatives of displacements w.r.t. time, and initialize deriviatives of velocities
    // w.r.t. time.
    tangent.segment(0, n_ * d) = state.segment(n_ * d, n_ * d);
    tangent.segment(n_ * d, n_ * d).setZero();

    // Initialize the jacobian. The upper left and lower right blocks will stay zero; the others are
    // set below.
    tangent_jacobian.setZero();

    // Set the upper right block of the jacobian. These are the derivatives of the tangent terms
    // calculated in the first line of this method.
    tangent_jacobian.block(0, n_ * d, n_ * d, n_ * d) = MatrixX<Scalar>::Identity(n_ * d, n_ * d);

    // This loop fills in the rest of the tangent and the lower left block of the jacobian.
    for (uint32_t i = 0; i < n_; ++i) {
        for (uint32_t j = i + 1; j < n_; ++j) {
            // Do some shared temp calculations.
            Eigen::Matrix<Scalar, d, 1> const displacement =
                state.segment<d>(j * d) - state.segment<d>(i * d);
            Scalar const distance_squared = displacement.squaredNorm();
            Scalar const distance = std::sqrt(distance_squared);
            Scalar const distance_5 = distance_squared * distance_squared * distance;

            // Compute the accelerations as before.
            Eigen::Matrix<Scalar, d, 1> const force =
                G * displacement / (distance_squared * distance);
            tangent.segment<d>((n_ + i) * d) += m_[j] * force;
            tangent.segment<d>((n_ + j) * d) -= m_[i] * force;

            // Compute shared expression in the dim * dim matrix of partial derivatives of the
            // accelerations w.r.t. to displacements (of mass i).
            Eigen::Matrix<Scalar, d, d> dforce;
            dforce.fill(3 * G * displacement.prod() / distance_5);
            dforce.diagonal() = (3 * displacement.array() - distance_squared).matrix() / distance_5;

            // Just multiply by masses and set sign to complete partial derivatives of the
            // accelerations w.r.t. displacement i, and w.r.t. displacement j.
            tangent_jacobian.block((n_ + i) * d, j * d, d, d) += m_[j] * dforce;
            tangent_jacobian.block((n_ + j) * d, i * d, d, d) -= m_[i] * dforce;
        }
    }
}

//..................................................................................................
void NBody::initialize(
    uint32_t dim,
    VectorX<Scalar> const& masses,
    VectorX<Scalar> const& state,
    Scalar delta_t
) {
    assert(2 * dim * masses.size() == state.size());

    law_.set_masses(masses);
    delta_t_ = delta_t;

    // initialize state
    t_ = 0;
    state_ = state;
    state_jacobian_.resize(state_.size(), state_.size());
    state_jacobian_.setIdentity();

    // initialize working memory
    tangent_.resize(state_.size());
    tangent_.setZero();
    tangent_jacobian_.resize(state_.size(), state_.size());
    tangent_jacobian_.setZero();

    points_.resize(law_.n() * law_.d);
    points_ = state_.segment(0, law_.n() * law_.d);

    //std::cout << "state.size() = " << state_.size() << '\n';
    //std::cout << "points.size() = " << points_.size() << '\n';
}

//..................................................................................................
void NBody::step() {
    diff_euler_step(*this, delta_t_, t_, state_, state_jacobian_);

    // hack
    points_ = state_.segment(0, law_.n() * law_.d);
}
