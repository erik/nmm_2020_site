#include "glut_grapher.h"
#include <thread>

//..................................................................................................
void GlutGrapher::initialize(
    SimulationInterface* sim,
    BoundingBox<Vector2<Scalar>> const& bounds
) {
    frame_ = 0;
    sim_ = sim;

    auto const extents = bounds.extents();

    aspect_ratio_ = extents[0] / extents[1];
    Scalar window_aspect_ratio = static_cast<Scalar>(max_width_) / max_height_;
    if (aspect_ratio_ >= window_aspect_ratio) {
        // Width is the limiting factor.
        window_size_[0] = max_width_;
        window_size_[1] = max_width_ / aspect_ratio_;
    } else {
        // Height is the limiting factor.
        window_size_[1] = max_height_;
        window_size_[0] = max_height_ * aspect_ratio_;
    }

    // Set the correct zoom and translation to fit the simulation into the window.
    translation_ = -bounds.low();
    zoom_ = 1 / extents[0];
}

//..................................................................................................
void GlutGrapher::init_glut() {
    int argc = 0;
    glutInit(&argc, nullptr);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(window_size_[0], window_size_[1]);
    glutCreateWindow("glimage");
    glShadeModel(GL_SMOOTH);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glClearColor(1, 1, 1, 0);
    gluOrtho2D(0, 1, 0, 1 / aspect_ratio_);
    glPointSize(5);
}

//..................................................................................................
void GlutGrapher::display() const {
    glClear(GL_COLOR_BUFFER_BIT);

    // Draw the function
    glColor3f(0, 0, 0);
    glBegin(GL_POINTS);
    VectorX<Scalar> const& points = sim_->points();
    for (int32_t i = 0; i < points.size(); i += 2) {
        Vector2<Scalar> const loc = zoom_ * (Vector2<Scalar>(points[i], points[i + 1]) + translation_);
        glVertex2f(loc[0], loc[1]);
    }
    glEnd();

    glFlush();
}

//..................................................................................................
void GlutGrapher::idle() {
    bool const wait_to_start = false;
    if (wait_to_start && frame_ == 0) {
        std::cout << "Press enter to start a 10 second countdown" << std::endl;
        do {} while (std::cin.get() != '\n');
        for (uint32_t i = 10; i > 0; --i) {
            std::cout << i << "... " << std::flush;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        std::cout << 0 << "... " << std::endl;
    }

    //std::cout << frame_ << ": ";
    //print_vector(sim_->points());
    for (uint32_t i = 0; i < 50; ++i) {
        sim_->step();
        ++frame_;
    }
    glutSwapBuffers();
    glutPostRedisplay();
}
