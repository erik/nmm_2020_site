#ifndef N_BODY_H
#define N_BODY_H

#include "glut_grapher.h"
#include "integrators.h"
#include "scalar.h"
#include "vector.h"

//--------------------------------------------------------------------------------------------------
// state vectors (i.e. phase space points) are packed as follows:
// 0, ..., d - 1 give (up to) x, y, z for mass zero
// d, ..., 2 * d - 1 give (up to) x, y, z for mass one
// ...
// (n - 1) * d, ..., n * d - 1 give (up to) x, y, z for mass n - 1
// n * d, ..., (n + 1) * d give (up to) v_x, v_y, v_z for mass zero
// ...
// (2 * n - 1) * d, ..., 2 * n * d - 1 give (up to) v_x, v_y, v_z for mass n - 1

//--------------------------------------------------------------------------------------------------
// Functor that computes derivatives of phase space coordinates w.r.t. time
class NBodyLaw {
public:
    NBodyLaw() {}
    void resize(uint32_t n);
    void set_masses(VectorX<Scalar> const& m);

    uint32_t n() const { return n_; }
    // Computes the derivatives of the phase space coordinates w.r.t. time at a given state. This is
    // a vector in the tangent bundle of phase space at the specified state, hence the name.
    void operator()(VectorX<Scalar> const& state, VectorX<Scalar>& tangent);
    // Computes the derivatives as above, as well as the jacobian of this tangent w.r.t. the phase
    // space coordinates at a given state.
    void operator()(
        VectorX<Scalar> const& state,
        VectorX<Scalar>& tangent,
        MatrixX<Scalar>& jacobian_tangent
    );

    static constexpr uint32_t d = 2;
    static constexpr Scalar G = 1e-1;

private:
    void compute_force();

    uint32_t n_; // number of particles
    VectorX<Scalar> m_;
};

//--------------------------------------------------------------------------------------------------
// N-Body simulation based on integration of NBodyLaw
class NBody : public SimulationInterface {
public:
    NBody() {}
    void initialize(
        uint32_t dim,
        VectorX<Scalar> const& masses,
        VectorX<Scalar> const& state,
        Scalar delta_t
    );
    void step() final;

    VectorX<Scalar> const& points() const final { return points_; }
    VectorX<Scalar> const& state() const { return state_; }
    MatrixX<Scalar> const& state_jacobian() const { return state_jacobian_; }

    // These methods make NBody compatible with the differentiable integrators.
    void operator()(Scalar, VectorX<Scalar> const& state) {
        law_(state, tangent_, tangent_jacobian_);
    }
    VectorX<Scalar> const& tangent() const { return tangent_; }
    MatrixX<Scalar> const& tangent_jacobian() const { return tangent_jacobian_; }

private:
    // configuration
    NBodyLaw law_;
    Scalar delta_t_;

    // state
    Scalar t_;
    // Holds the current state (displacements and velocities).
    VectorX<Scalar> state_;
    // Holds the jacobian of current state variables w.r.t. initial state variables.
    MatrixX<Scalar> state_jacobian_;

    // working memory (temporary)
    // Holds the tangent of state w.r.t time (i.e. velocities and accelerations).
    VectorX<Scalar> tangent_;
    // Holds the jacobian of tangent_ w.r.t. state variables (displacements and velocities).
    MatrixX<Scalar> tangent_jacobian_;

    // copy of first half of state_, just to make visualization easy
    VectorX<Scalar> points_;
};

#endif
