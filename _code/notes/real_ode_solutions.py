from sympy import *
from sympy.assumptions.assume import global_assumptions

def print_expr(name, expr):
    print(name + ": ")
    pprint(expr)
    print("")

# Actual basic symbols
t = symbols('t', real=True)
w = symbols('w', real=True, positive=True)
a_1 = symbols('a_1', real=True)
a_2 = symbols('a_2', real=True)
b_1 = symbols('b_1', real=True)
b_2 = symbols('b_2', real=True)
phi_1 = symbols('phi_1', real=True)
phi_2 = symbols('phi_2', real=True)

x = (a_1 + I * a_2) * exp(I * w * t) + (b_1 + I * b_2) * exp(-I * w * t)
pprint(simplify(re(x)))
pprint(simplify(im(x)))

x = (a_1 * exp(I * w * phi_1)) * exp(I * w * t) + (b_1 * exp(I * w * phi_2)) * exp(-I * w * t)
pprint(simplify(re(x)))
pprint(simplify(im(x)))
