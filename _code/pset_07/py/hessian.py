from sympy import *


a = Symbol("a", real=True)
b = Symbol("b", real=True)
x = Symbol("x", real=True)
y = Symbol("y", real=True)

error = (y - sin(a + b * x))**2

error_da = simplify(diff(error, a))
error_db = simplify(diff(error, b))
print(latex(error_da))
print(latex(error_db))

error_daa = simplify(diff(error_da, a))
error_dab = simplify(diff(error_da, b))
error_dba = simplify(diff(error_db, a))
error_dbb = simplify(diff(error_db, b))
print(latex(error_daa))
print(latex(error_dab))
print(latex(error_dba))
print(latex(error_dbb))
