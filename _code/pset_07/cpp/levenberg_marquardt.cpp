#include "xorshift.h"
#include <Eigen/Dense>
#include <iostream>
#include <random>
#include <vector>

using namespace std;
using namespace Eigen;

//--------------------------------------------------------------------------------------------------
XorShift64 rg1;
std::random_device rd{};
std::mt19937 rg2{rd()};

//--------------------------------------------------------------------------------------------------
Eigen::ArrayXd draw_n_uniform(uint32_t n) {
    Eigen::ArrayXd result(n);
    for (uint32_t i = 0; i < n; ++i) {
        result[i] = rg1.draw_double();
    }
    return result;
}

//--------------------------------------------------------------------------------------------------
// TODO Box-Muller transform my uniform samples instead of using STL
Eigen::ArrayXd draw_n_normal(uint32_t n, double std_deviation) {
    std::normal_distribution<> normal{0, std_deviation};
    Eigen::ArrayXd result(n);
    for (uint32_t i = 0; i < n; ++i) {
        result[i] = normal(rg2);
    }
    return result;
}

//--------------------------------------------------------------------------------------------------
int main() {
    uint32_t constexpr n_warmup = 1000;
    for (uint32_t i = 0; i < n_warmup; ++i) {
        rg1.advance();
    }

    uint32_t constexpr n_samples = 100;
    double const std_deviation = 0.1;
    Eigen::ArrayXd samples = draw_n_uniform(n_samples);
    Eigen::ArrayXd errors = draw_n_normal(n_samples, std_deviation);
    //Eigen::ArrayXd values = sin(2 + 3 * samples) + errors;
    Eigen::ArrayXd values = sin(2 + 3 * samples);

    double a = 1;
    double b = 1;
    double lambda = 10;
    constexpr bool vary_lambda = false;
    double error = (values - sin(a + b * samples)).matrix().squaredNorm();
    double new_error;
    Eigen::Vector2d grad;
    Eigen::Matrix2d mat;
    Eigen::ArrayXd tmp(n_samples);
    Eigen::Vector2d step;
    std::cout << "error: " << error << '\n';

    for (uint32_t i = 0; i < 1000; ++i) {
        // compute gradient
        tmp = (values - sin(a + b * samples)) * cos(a + b * samples);
        grad[0] = -2 * tmp.sum();
        grad[1] = -2 * (samples * tmp).sum();

        // compute levenberg marquardt matrix (based on hessian)
        tmp = values * sin(a + b * samples) + cos(2 * (a + b * samples));
        mat(0, 0) = tmp.sum() + lambda;
        mat(0, 1) = (samples * tmp).sum();
        mat(1, 0) = mat(0, 1);
        mat(1, 1) = (samples * samples * tmp).sum() + lambda;

        // compute the step
        step = -mat.inverse() * grad; // Levenberg-Marquardt
        //step = -lambda * grad;      // gradient descent

        // apply the step
        a += step[0];
        b += step[1];
        new_error = (values - sin(a + b * samples)).matrix().squaredNorm();

        if (vary_lambda) {
            if (new_error < error) {
                lambda *= 0.8;
            } else {
                lambda *= 1.2;
            }
        }
        error = new_error;

        //std::cout << "error: " << error << '\n';
    }

    std::cout << "error: " << error << '\n';
    std::cout << "a = " << a << ", b = " << b << '\n';
}
