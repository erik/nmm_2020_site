#include <Eigen/Dense>
#include <iostream>
#include <vector>
#include "xorshift.h"

//--------------------------------------------------------------------------------------------------
int main() {
    uint32_t constexpr n_warmup = 1000;
    uint32_t constexpr n_samples = 100000;
    uint32_t constexpr n_printed = 1000;

    XorShift64 rand;
    for (uint32_t i = 0; i < n_warmup; ++i) {
        rand.advance();
    }

    Eigen::ArrayX2d samples;
    samples.resize(n_samples, 2);

    // Draw uniform samples.
    for (uint32_t i = 0; i < n_samples; ++i) {
        samples(i, 0) = rand.draw_double();
        samples(i, 1) = rand.draw_double();
    }
    samples.col(1) *= 2 * 3.1415926;

    // Print uniform samples.
    std::cout << "Uniform samples\n";
    for (uint32_t i = 0; i < n_printed; ++i) {
        std::cout << '[' << samples(i, 0) << ", " << samples(i, 1) << "], ";
    }
    std::cout << "\n\n";

    // Transform samples.
    Eigen::ArrayXd radii = Eigen::sqrt(-2 * Eigen::log(samples.col(0)));
    samples.col(0) = radii * Eigen::sin(samples.col(1));
    samples.col(1) = radii * Eigen::cos(samples.col(1));

    // Print transformed samples.
    std::cout << "Transformed samples\n";
    for (uint32_t i = 0; i < n_printed; ++i) {
        std::cout << '[' << samples(i, 0) << ", " << samples(i, 1) << "], ";
    }
    std::cout << "\n\n";

    // Calculate expectations of y_1, y_1^2, and y_1^3.
    double e_1 = samples.col(1).mean();
    double e_2 = samples.col(1).square().mean();
    double e_3 = samples.col(1).cube().mean();
    std::cout << "C1 = " << e_1 << '\n';
    std::cout << "C2 = " << e_2 - e_1 * e_1 << '\n';
    std::cout << "C3 = " << e_3 - 3 * e_1 * e_2 + 2 * e_1 * e_1 * e_1 << '\n';
}
