#include <Eigen/Dense>
#include <array>
#include <iostream>
#include <iomanip>
#include "xorshift.h"

//--------------------------------------------------------------------------------------------------
int main() {
    /*
    uint32_t constexpr n_warmup = 1000;
    uint32_t constexpr n_steps = 1000;
    uint32_t constexpr n_trajectories = 10;

    XorShift64 rand;
    for (uint32_t i = 0; i < n_warmup; ++i) {
        rand.advance();
    }

    auto rand_step = [&rand]() -> int32_t {
        return static_cast<int32_t>((rand.draw_uint32() & 0x1) << 1) - 1;
    };

    int32_t pos;
    for (uint32_t i = 0; i < n_trajectories; ++i) {
        pos = 0;
        std::cout << "trajectory_" << i << " = np.array([0";
        for (uint32_t j = 1; j < n_steps; ++j) {
            pos += rand_step();
            std::cout << ", " << pos;
        }
        std::cout << "])\n";
    }
    */

    int32_t constexpr n_steps = 1001;
    auto const max_valid_pos = [](int32_t n) -> int32_t {
        return std::floor(1.5 * std::sqrt(static_cast<double>(n)));
    };

    // 3 since we add one for the middle, and one on either end
    int32_t data_width = 2 * max_valid_pos(n_steps - 1) + 3;
    Eigen::ArrayXXd data;
    data.resize(n_steps, data_width);
    data.fill(0);

    int32_t const max_pos = max_valid_pos(n_steps - 1) + 1;
    auto const get_index = [=](int32_t n) -> int32_t {
        return n + max_pos;
    };

    data(0, get_index(0)) = 1;
    for (int32_t i = 1; i < n_steps; ++i) {
        int32_t const max_valid = max_valid_pos(i);
        for (int32_t j = -max_valid; j <= max_valid; ++j) {
            data(i, get_index(j)) = 0.5 * (data(i - 1, get_index(j - 1)) + data(i - 1, get_index(j + 1)));
        }
    }

    //for (int32_t i = -48; i <= 48; ++i) {
    //    std::cout << data(n_steps - 1, get_index(i)) << '\n';
    //}
    //std::cout << '\n';

    std::cout << "props = np.array([" << data.row(0).sum();
    for (int32_t i = 1; i < n_steps; ++i) {
        std::cout << ", " << data.row(i).sum();
    }
    std::cout << "])\n";
}
