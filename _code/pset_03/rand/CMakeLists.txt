add_executable(rand1
    main1.cpp
)
target_link_libraries(rand1 shared_settings shared_code Eigen3::Eigen)

add_executable(rand2
    main2.cpp
)
target_link_libraries(rand2 shared_settings shared_code Eigen3::Eigen)
