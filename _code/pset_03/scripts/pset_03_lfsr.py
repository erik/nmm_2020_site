import numpy as np

state = np.array([0, 0, 0, 1])
print(state)

for i in range(1, 16):
    next = (state[0] + state[3]) % 2
    state = np.roll(state, 1)
    state[0] = next
    print(state)
