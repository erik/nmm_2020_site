import numpy as np
import matplotlib.pyplot as plt
import matplotlib.collections as mc

# These diagrams are for 6.2 (a)

def make_square():
    pts = np.array([[0, 0], [0, 2], [2, 2], [2, 0]])
    lines = np.array([
        [pts[0], pts[1]],
        [pts[1], pts[2]],
        [pts[2], pts[3]],
        [pts[3], pts[0]],
    ])
    fig = plt.figure(figsize=[2, 2])
    left, bottom, width, height = 0.15, 0.1, 0.8, 0.8
    ax = fig.add_axes([left, bottom, width, height])
    ax.axis('off')
    ax.scatter(pts[:, 0], pts[:, 1], s = 100, color='k')
    ax.add_collection(mc.LineCollection(lines, linewidths=2, colors='k'))
    ax.annotate("$dx_1$", (1, 0), textcoords="offset points", xytext=(0, -20), ha="center")
    ax.annotate("$dx_2$", (0, 1), textcoords="offset points", xytext=(-10, 0), va="center", ha="right")
    fig.savefig("../../../assets/img/03_square.png", transparent=True)
    #plt.show(fig)

def make_parallelogram():
    pts = np.array([[0, 0], [4, 3], [1, 4], [5, 7]])
    bounds = np.array([[0, 0], [5, 0], [5, 7], [0, 7]])
    midpoints = np.array([[4, 0], [5, 3], [0, 4], [1, 7]])

    solid_lines = np.array([
        [pts[0], pts[1]],
        [pts[1], pts[3]],
        [pts[3], pts[2]],
        [pts[2], pts[0]],
    ])
    dotted_lines = np.array([
        [bounds[0], bounds[1]],
        [bounds[1], bounds[2]],
        [bounds[2], bounds[3]],
        [bounds[3], bounds[0]],
        [midpoints[0], pts[1]],
        [pts[1], midpoints[1]],
        [midpoints[2], pts[2]],
        [pts[2], midpoints[3]],
    ])

    fig = plt.figure()
    left, bottom, width, height = 0.15, 0.1, 0.8, 0.85
    ax = fig.add_axes([left, bottom, width, height])
    ax.axis('off')
    ax.scatter(pts[:, 0], pts[:, 1], s = 100, color='k')
    ax.scatter(midpoints[:, 0], midpoints[:, 1], s = 20, color='k')
    ax.scatter([5, 0], [0, 7], s = 20, color='k')
    ax.add_collection(mc.LineCollection(solid_lines, linewidths=2, colors='k'))
    ax.add_collection(mc.LineCollection(dotted_lines, linewidths=2, colors='k', linestyle='dotted'))
    ax.annotate("$(∂y_1/∂x_1) dx_1$", (2, 0), textcoords="offset points", xytext=(0, -20), ha="center")
    ax.annotate("$(∂y_1/∂x_2) dx_2$", (4.5, 0), textcoords="offset points", xytext=(0, -20), ha="center")
    ax.annotate("$(∂y_2/∂x_2) dx_2$", (0, 2), textcoords="offset points", xytext=(-10, 0), va="center", ha="right")
    ax.annotate("$(∂y_2/∂x_1) dx_1$", (0, 5), textcoords="offset points", xytext=(-10, 0), va="center", ha="right")
    fig.savefig("../../../assets/img/03_parallelogram.png", transparent=True)
    #plt.show(fig)

make_square()
make_parallelogram()
