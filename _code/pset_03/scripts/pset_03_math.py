from sympy import *
from sympy.assumptions.assume import global_assumptions

def print_expr(name, expr):
    print(name + ": ")
    pprint(expr)
    print("")

# This is for 6.2 (b).

def forward_jacobian():
    # define basic symbols
    x1 = symbols('x_1', real=True)
    x2 = symbols('x_2', real=True)

    y1 = sqrt(-2 * log(x1)) * sin(x2)
    y2 = sqrt(-2 * log(x1)) * cos(x2)

    dy1_dx1 = simplify(diff(y1, x1))
    dy1_dx2 = simplify(diff(y1, x2))
    dy2_dx1 = simplify(diff(y2, x1))
    dy2_dx2 = simplify(diff(y2, x2))

    print_expr('dy1_dx1', dy1_dx1)
    print_expr('dy1_dx2', dy1_dx2)
    print_expr('dy2_dx1', dy2_dx1)
    print_expr('dy2_dx2', dy2_dx2)

def inverse_jacobian():
    # define basic symbols
    y1 = symbols('y_1', real=True)
    y2 = symbols('y_2', real=True)

    x1 = exp(-(y1**2 + y2**2) / 2)
    x2 = atan2(y1, y2)

    dx1_dy1 = simplify(diff(x1, y1))
    dx1_dy2 = simplify(diff(x1, y2))
    dx2_dy1 = simplify(diff(x2, y1))
    dx2_dy2 = simplify(diff(x2, y2))

    print_expr('dx1_dy1', dx1_dy1)
    print_expr('dx1_dy2', dx1_dy2)
    print_expr('dx2_dy1', dx2_dy1)
    print_expr('dx2_dy2', dx2_dy2)

if __name__ == "__main__":
    #forward_jacobian()
    inverse_jacobian()
