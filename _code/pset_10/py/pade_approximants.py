import matplotlib.pyplot as plt
import numpy as np
import sympy as sp

x = sp.symbols("x", real=True)

def pade_approximant(function, N, M):
    # Compute relevant taylor series terms
    derivative = function
    taylor_coefficients = [ function.subs(x, 0) ]
    for i in range(1, N + M + 1):
        derivative = sp.diff(derivative, x)
        taylor_coefficients.append(derivative.subs(x, 0) / sp.factorial(i))

    # Build matrix
    one_one = [1] + [0] * M
    matrix_rows = [one_one]
    for i in range(1, M + 1):
        new_row = []
        for j in range(0, min(N + i, M) + 1):
            new_row.append(taylor_coefficients[N + i - j])
        for j in range(min(N + i, M) + 1, M + 1):
            new_row.append(0)
        matrix_rows.append(new_row)
    A = sp.Matrix(matrix_rows)
    b = sp.Matrix(one_one)
    sp.pprint(A)
    sp.pprint(b)

    # Solve
    answer = sp.linsolve((A, b))
    b_coeffs = list(answer.args[0])

    a_coeffs = [
        sum(b_coeffs[m] * taylor_coefficients[n - m] for m in range(0, min(n, M) + 1))
        for n in range(0, N + 1)
    ]

    print(a_coeffs)
    print(b_coeffs)
    print("")

    return lambda x_val: sum(a_coeffs[n] * x_val ** n for n in range(0, N + 1)) \
        / sum(b_coeffs[m] * x_val ** m for m in range(0, M + 1))

# print pade approximant values
function = sp.exp(x)
pade_approximations = [ pade_approximant(function, i, i) for i in range(1, 6) ]
for approx in pade_approximations:
    print(approx(1))
print("")
for approx in pade_approximations:
    print(approx(1.0))
print("")
pade_errors = [ abs(approx(1.0) - function.subs(x, 1.0)) for approx in pade_approximations ]

# Create polynomial approximations and print their values
poly_approximations = [
    #lambda x_val: sum(taylor_coefficients[n] * x**n for n in range(0, order)) for order in [3, 5, 7, 9, 11]
    sum(x**n / sp.factorial(n) for n in range(0, order)) for order in [3, 5, 7, 9, 11]
]
for approx in poly_approximations:
    print(approx)
    print(approx.subs(x, 1))
    print(approx.subs(x, 1.0))
    print("")
poly_errors = [ abs(approx.subs(x, 1.0) - function.subs(x, 1.0)) for approx in poly_approximations ]

# Graph errors
fig1 = plt.figure()
left, bottom, width, height = 0.1, 0.1, 0.8, 0.8
ax1 = fig1.add_axes([left, bottom, width, height])
x_vals = [3, 5, 7, 9, 11] # number of free parameters
ax1.set_yscale("log")
ax1.plot(x_vals, pade_errors, label="padé")
ax1.plot(x_vals, poly_errors, label="poly")
ax1.set_xlabel("free parameters")
ax1.set_ylabel("absolute error")
ax1.legend()
ax1.set_title("Absolute errors of Padé and polynomial approximations")
fig1.savefig("../../../assets/img/10_errors.png", transparent=True)
