from sympy import *
from sympy.assumptions.assume import global_assumptions

# used as a generic variable, i.e. for polynomials, integration, etc.
x = Symbol("x", real=True)
# defines the width of a quantized segment
h = Symbol("h", real=True, positive=True)
# used to refer to the number of segments (so there are n + 1 points)
n = Symbol("n", real=True, positive=True)
# used as a generic index for an interior segment (i.e. not touching the boundary)
k = Symbol("k", real=True, positive=True)

# not sure why I bothered with this
def linear_hats():
    n = 5

    A = zeros(5, 5)
    for i in range(0, n):
        if i - 1 >= 0:
            A[i, i - 1] = h / 6
        A[i, i] = 2 * h / 3
        if i + 1 < n:
            A[i, i + 1] = h / 6
    print(latex(A))

    B = zeros(5, 5)
    for i in range(0, n):
        if i - 1 >= 0:
            B[i, i - 1] = 1 / h
        B[i, i] = -2 / h
        if i + 1 < n:
            B[i, i + 1] = 1 / h
    print(latex(B))

# Assumes both functions passed in are nonzero only on [0, h].
class LocalBasisFunction:
    def __init__(self, left, right):
        self.left = left
        self.right = right
        self.d_left = diff(left, x)
        self.d_right = diff(right, x)

    def left_value(self, xval):
        return self.left.subs(x, xval)

    def left_slope(self, xval):
        return self.d_left.subs(x, xval)

    def right_value(self, xval):
        return self.right.subs(x, xval)

    def right_slope(self, xval):
        return self.d_right.subs(x, xval)

def cubic_basis_functions():
    # helps determine cubic poly coefficients
    poly_matrix = Matrix([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [1, h, h**2, h**3],
        [0, 1, 2*h, 3*h**2]
    ])
    poly_matrix_inverse = poly_matrix**-1
    print(latex(poly_matrix))
    print(latex(poly_matrix_inverse))
    print("")

    p_1 = (poly_matrix_inverse * Matrix([1, 0, 0, 0])).dot(Matrix([1, x, x**2, x**3]))
    p_2 = (poly_matrix_inverse * Matrix([0, 1, 0, 0])).dot(Matrix([1, x, x**2, x**3]))
    p_3 = (poly_matrix_inverse * Matrix([0, 0, 1, 0])).dot(Matrix([1, x, x**2, x**3]))
    p_4 = (poly_matrix_inverse * Matrix([0, 0, 0, 1])).dot(Matrix([1, x, x**2, x**3]))
    print(latex(p_1))
    print(latex(p_2))
    print(latex(p_3))
    print(latex(p_4))
    print("")

    return [
        LocalBasisFunction(p_3, p_1),
        LocalBasisFunction(p_4, p_2)
    ]

# Computes the matrix A_{i, j} = integrate(f1_i * f2_j, (x, 0, h)). Both arguments must be
# LocalBasisFunctions. Note that A is tridiagonal.
def compute_A(f1, f2):
    result = {}

    # Compute the diagonal elements. The first and last are special cases since the basis function
    # is chopped in half.
    first_diag = integrate(f1.right * f2.right, (x, 0, h))
    last_diag = integrate(f1.left * f2.left, (x, 0, h))
    diag = first_diag + last_diag
    result[0, 0] = first_diag
    result[k, k] = diag
    result[n, n] = last_diag

    # Compute off diagonal elements. These entries will always be the same if f1 and f2 are the
    # same. But they need not be in general.
    above_diag = integrate(f1.right * f2.left, (x, 0, h))
    below_diag = integrate(f1.left * f2.right, (x, 0, h))
    result[k, k + 1] = above_diag
    result[k, k - 1] = below_diag

    return result

# Computes the matrix B_{i, j} = [f1_i * f2_j']_0^1 - integrate(f1_i' * f2_j', (x, 0, h)). Both
# arguments must be LocalBasisFunctions. Note that B is tridiagonal.
def compute_B(f1, f2):
    result = {}

    # Compute the diagonal elements. The first and last are special cases since the basis function
    # is chopped in half, and they have boundary contributions.
    left_integral = -integrate(f1.d_left * f2.d_left, (x, 0, h))
    right_integral = -integrate(f1.d_right * f2.d_right, (x, 0, h))
    diag = left_integral + right_integral
    first_diag = -f1.right_value(0) * f2.right_slope(0) + right_integral
    last_diag = f1.left_value(h) * f2.left_slope(h) + left_integral
    result[0, 0] = first_diag
    result[k, k] = diag
    result[n, n] = last_diag

    # Compute off diagonal elements. These entries will always be the same if f1 and f2 are the
    # same. But they need not be in general.
    above_diag = -integrate(f1.d_right * f2.d_left, (x, 0, h))
    below_diag = -integrate(f1.d_left * f2.d_right, (x, 0, h))
    result[k, k + 1] = above_diag
    result[k, k - 1] = below_diag

    return result

def render_matrix(entries, size):
    result = zeros(size, size)
    for i in range(0, size):
        result[i, i] = entries[(k, k)]
    for i in range(0, size - 1):
        result[i, i + 1] = entries[(k, k + 1)]
    for i in range(1, size):
        result[i, i - 1] = entries[(k, k - 1)]
    if (0, 0) in entries:
        result[0, 0] = entries[(0, 0)]
    if (n, n) in entries:
        result[size - 1, size - 1] = entries[(n, n)]
    if (0, 1) in entries:
        result[0, 1] = entries[(0, 1)]
    if (1, 0) in entries:
        result[1, 0] = entries[(1, 0)]
    return result

if __name__ == "__main__":
    #linear_hats()

    basis_functions = cubic_basis_functions()
    psi = basis_functions[0]
    zeta = basis_functions[1]

    A_psi_psi = compute_A(psi, psi)
    for coords, value in A_psi_psi.items():
        print(f"A^{{\\psi, \\psi}}_{{{coords[0], coords[1]}}} &= " + latex(value) + " \\\\")
    print(latex(render_matrix(A_psi_psi, 5)))

    A_zeta_zeta = compute_A(zeta, zeta)
    for coords, value in A_zeta_zeta.items():
        print(f"A^{{\\zeta, \\zeta}}_{{{coords[0], coords[1]}}} &= " + latex(value) + " \\\\")
    print(latex(render_matrix(A_zeta_zeta, 5)))

    A_psi_zeta = compute_A(psi, zeta)
    for coords, value in A_psi_zeta.items():
        print(f"A^{{\\psi, \\zeta}}_{{{coords[0], coords[1]}}} &= " + latex(value) + " \\\\")
    print(latex(render_matrix(A_psi_zeta, 5)))

    print("")

    B_psi_psi = compute_B(psi, psi)
    for coords, value in B_psi_psi.items():
        print(f"B^{{\\psi, \\psi}}_{{{coords[0], coords[1]}}} &= " + latex(value) + " \\\\")
    print(latex(render_matrix(B_psi_psi, 5)))

    B_zeta_zeta = compute_B(zeta, zeta)
    for coords, value in B_zeta_zeta.items():
        print(f"B^{{\\zeta, \\zeta}}_{{{coords[0], coords[1]}}} &= " + latex(value) + " \\\\")
    print(latex(render_matrix(B_zeta_zeta, 5)))

    B_psi_zeta = compute_B(psi, zeta)
    for coords, value in B_psi_zeta.items():
        print(f"B^{{\\psi, \\zeta}}_{{{coords[0], coords[1]}}} &= " + latex(value) + " \\\\")
    print(latex(render_matrix(B_psi_zeta, 5)))

    B_zeta_psi = compute_B(zeta, psi)
    for coords, value in B_zeta_psi.items():
        print(f"B^{{\\zeta, \\psi}}_{{{coords[0], coords[1]}}} &= " + latex(value) + " \\\\")
    print(latex(render_matrix(B_zeta_psi, 5)))

    print("")
