import numpy as np
from sympy import *
from sympy.assumptions.assume import global_assumptions
import matplotlib.pyplot as plt

# used as a generic variable, i.e. for polynomials, integration, etc.
x = Symbol("x", real=True)
# defines the width of a quantized segment
h = Symbol("h", real=True, positive=True)
# used to refer to the number of segments (so there are n + 1 points)
n = Symbol("n", real=True, positive=True)
# used as a generic index for an interior segment (i.e. not touching the boundary)
k = Symbol("k", real=True, positive=True)
# used to specify the applied force
f = Symbol("f", real=True, positive=True)

# Assumes both functions passed in are nonzero only on [0, h].
class LocalBasisFunction:
    def __init__(self, left, right):
        self.left = left
        self.right = right
        self.d_left = diff(self.left, x)
        self.d_right = diff(self.right, x)
        self.dd_left = diff(self.d_left, x)
        self.dd_right = diff(self.d_right, x)

    def left_value(self, xval):
        return self.left.subs(x, xval)

    def left_slope(self, xval):
        return self.d_left.subs(x, xval)

    def left_curvature(self, xval):
        return self.dd_left.subs(x, xval)

    def right_value(self, xval):
        return self.right.subs(x, xval)

    def right_slope(self, xval):
        return self.d_right.subs(x, xval)

    def right_curvature(self, xval):
        return self.dd_right.subs(x, xval)

def cubic_basis_functions():
    # helps determine cubic poly coefficients
    poly_matrix = Matrix([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [1, h, h**2, h**3],
        [0, 1, 2*h, 3*h**2]
    ])
    poly_matrix_inverse = poly_matrix**-1
    print(latex(poly_matrix))
    print(latex(poly_matrix_inverse))
    print("")

    p_1 = (poly_matrix_inverse * Matrix([1, 0, 0, 0])).dot(Matrix([1, x, x**2, x**3]))
    p_2 = (poly_matrix_inverse * Matrix([0, 1, 0, 0])).dot(Matrix([1, x, x**2, x**3]))
    p_3 = (poly_matrix_inverse * Matrix([0, 0, 1, 0])).dot(Matrix([1, x, x**2, x**3]))
    p_4 = (poly_matrix_inverse * Matrix([0, 0, 0, 1])).dot(Matrix([1, x, x**2, x**3]))
    print(latex(p_1))
    print(latex(p_2))
    print(latex(p_3))
    print(latex(p_4))
    print("")

    return [
        LocalBasisFunction(p_3, p_1),
        LocalBasisFunction(p_4, p_2)
    ]

# Computes the matrix A_{i, j} = integrate(f1_i'' * f2_j'', (x, 0, h)). Both arguments must be
# LocalBasisFunctions. Note that A is tridiagonal.
def compute_A(f1, f2):
    result = {}

    # Compute the diagonal elements. The first and last are special cases since the basis function
    # is chopped in half.
    first_diag = integrate(f1.dd_right * f2.dd_right, (x, 0, h))
    last_diag = integrate(f1.dd_left * f2.dd_left, (x, 0, h))
    diag = first_diag + last_diag
    result[0, 0] = first_diag
    result[k, k] = diag
    result[n, n] = last_diag

    # Compute off diagonal elements. These entries will always be the same if f1 and f2 are the
    # same. But they need not be in general.
    above_diag = integrate(f1.dd_right * f2.dd_left, (x, 0, h))
    below_diag = integrate(f1.dd_left * f2.dd_right, (x, 0, h))
    result[k, k + 1] = above_diag
    result[k, k - 1] = below_diag

    return result

def compute_b(basis_function, force, n_nodes, n_loaded_nodes):
    result = [sympify(0)] * (n_nodes - n_loaded_nodes)
    left_integral = integrate(basis_function.left, (x, 0, h))
    right_integral = integrate(basis_function.right, (x, 0, h))
    result += [force * right_integral]
    if n_loaded_nodes > 2:
        result += [force * (left_integral + right_integral)] * (n_loaded_nodes - 2)
    result += [force * left_integral]
    return result

def make_block(f1, f2, n_nodes):
    block = compute_A(f1, f2)
    block = render_matrix(block, n_nodes)
    if n_nodes < 8:
        print(latex(block))
    block[0, 0] = 1 if f1 == f2 else 0
    for i in range(1, n_nodes):
        block[0, i] = 0
    if n_nodes < 8:
        print(latex(block))
        print("")
    return block

def render_matrix(entries, size):
    result = zeros(size, size)
    for i in range(0, size):
        result[i, i] = entries[(k, k)]
    for i in range(0, size - 1):
        result[i, i + 1] = entries[(k, k + 1)]
    for i in range(1, size):
        result[i, i - 1] = entries[(k, k - 1)]
    if (0, 0) in entries:
        result[0, 0] = entries[(0, 0)]
    if (n, n) in entries:
        result[size - 1, size - 1] = entries[(n, n)]
    if (0, 1) in entries:
        result[0, 1] = entries[(0, 1)]
    if (1, 0) in entries:
        result[1, 0] = entries[(1, 0)]
    return result

# basis_functions is a list of LocalBasisFunction objects
# coefficients is a 2d numpy array; each row is for a basis function, each col is for a node
def plot_result(name, basis_functions, coefficients, show_derivs = False):
    assert(len(basis_functions) == coefficients.shape[0])

    n_subdivs = 10
    xvals = []
    yvals = []
    dyvals = []
    ddyvals = []
    for i in range(0, coefficients.shape[1] - 1):
        sd = n_subdivs
        ep = False
        if i == coefficients.shape[1] - 2:
            sd += 1
            ep = True
        for xval in np.linspace(0, 1, sd, endpoint=ep):
            y = 0
            dy = 0
            ddy = 0
            for basis_function, row in zip(basis_functions, coefficients):
                y   += row[i]     * basis_function.right_value(xval).subs(h, 1)
                y   += row[i + 1] * basis_function.left_value(xval).subs(h, 1)
                if not show_derivs:
                    continue
                dy  += row[i]     * basis_function.right_slope(xval).subs(h, 1)
                dy  += row[i + 1] * basis_function.left_slope(xval).subs(h, 1)
                ddy += row[i]     * basis_function.right_curvature(xval).subs(h, 1)
                ddy += row[i + 1] * basis_function.left_curvature(xval).subs(h, 1)
            xvals.append(i + xval)
            yvals.append(y)
            dyvals.append(dy)
            ddyvals.append(ddy)

    fig1 = plt.figure()
    left, bottom, width, height = 0.1, 0.1, 0.8, 0.8
    ax1 = fig1.add_axes([left, bottom, width, height])
    ax1.plot(xvals, yvals, label="displacement")
    if show_derivs:
        ax1.plot(xvals, dyvals, label="slope")
        ax1.plot(xvals, ddyvals, label="curvature")
    ax1.set_xlabel('h')
    ax1.set_title(name)
    ax1.legend()
    #plt.show(fig1)
    fig1.savefig(f"../../../assets/img/06_{name}.png", transparent=True)

if __name__ == "__main__":
    # define simulation parameters
    # these are the ones I modified to produce different charts
    n_nodes = 5
    n_loaded_nodes = 3
    displacement_0 = 0
    slope_0 = 0.5
    force = -10.0

    # these I left constant
    length = 1.0
    # n nodes means n - 1 intervals
    # there are 2 * n_intervals degrees of freedom (we fix the coefficients for the first node)
    n_intervals = n_nodes - 1
    hval = length / n_intervals
    ei = 1.0

    print(f"{n_nodes} nodes")
    print(f"{n_intervals} elements")
    print(f"h = {hval}")
    print(f"E*I = {ei}")
    print(f"force = {force}")
    print(f"n_loaded_nodes = {n_loaded_nodes}")
    print(f"fixed displacement = {displacement_0}")
    print(f"fixed slope = {slope_0}")
    print("")

    # construct basis functions
    basis_functions = cubic_basis_functions()
    psi = basis_functions[0]
    zeta = basis_functions[1]
    #plot_result("psi", basis_functions, np.array([[0, 1, 0], [0, 0, 0]]), show_derivs=True)
    #plot_result("zeta", basis_functions, np.array([[0, 0, 0], [0, 1, 0]]), show_derivs=True)

    # construct blocks of A
    A_pp = make_block(psi, psi, n_nodes)
    A_zz = make_block(zeta, zeta, n_nodes)
    A_pz = make_block(psi, zeta, n_nodes)
    A_zp = make_block(zeta, psi, n_nodes)

    # convert matrices to numpy arrays
    A_pp = A_pp.subs(h, hval)
    A_pp = np.array(A_pp).astype(np.float64)
    A_zz = A_zz.subs(h, hval)
    A_zz = np.array(A_zz).astype(np.float64)
    A_pz = A_pz.subs(h, hval)
    A_pz = np.array(A_pz).astype(np.float64)
    A_zp = A_zp.subs(h, hval)
    A_zp = np.array(A_zp).astype(np.float64)

    # stack into complete matrix
    A = ei * np.block([[A_pp, A_pz], [A_zp, A_zz]])
    if n_nodes < 8:
        print(A)
        print("")

    # compute b
    b_p = compute_b(psi, f, n_nodes, n_loaded_nodes)
    b_z = compute_b(zeta, f, n_nodes, n_loaded_nodes)
    if n_nodes < 8:
        print(latex(b_p))
        print(latex(b_z))
    b_p[0] = sympify(displacement_0)
    b_z[0] = sympify(slope_0)
    if n_nodes < 8:
        print(latex(b_p))
        print(latex(b_z))
    b = b_p + b_z
    b = [ expr.subs([(h, hval), (f, force)]) for expr in b ]
    b = np.array(b).astype(np.float64)
    if n_nodes < 8:
        print("")

    a = np.linalg.solve(A, b)
    if n_nodes < 8:
        print(a)
        print("")

    plot_result(f"{n_nodes}_nodes", basis_functions, a.reshape((2, -1)), show_derivs=(n_nodes < 8))
