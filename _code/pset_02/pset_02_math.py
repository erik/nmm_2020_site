from sympy import *
from sympy.assumptions.assume import global_assumptions

def print_expr(name, expr):
    print(name + ": ")
    pprint(expr)
    print("")

# This is for 3.1 (d).

# define basic symbols
t = symbols('t', real=True)
m = symbols('m', real=True, positive=True)
k = symbols('k', real=True, positive=True)
gamma = symbols('gamma', real=True, positive=True)

# define derived quantities
omega_0 = sqrt(k / m)
alpha = gamma / (2 * m)
omega_1 = sqrt(omega_0**2 - alpha**2)

# define coefficients of general solution
A = symbols('A', real=True)
B = symbols('B', real=True)

# define and verify the general real solution
x = exp(-alpha * t) * (A * cos(omega_1 * t) + B * sin(omega_1 * t))
dx = simplify(diff(x, t))
ddx = simplify(diff(dx, t))
print_expr('x', x)
print_expr('dx', dx)
print_expr('ddx', ddx)
print_expr('zero (test)', simplify(m * ddx + gamma * dx + k * x))

# now that we know the solutions are correct, let's use fewer symbols
alpha = symbols('alpha', real=True, postiive=True)
omega_1 = symbols('omega_1', real=True, postiive=True)
x = exp(-alpha * t) * (A * cos(omega_1 * t) + B * sin(omega_1 * t))
dx = simplify(diff(x, t))
x_squared = simplify(x * x)
dx_squared = simplify(dx * dx)
print_expr('dx', dx)
print_expr('x^2', x_squared)
print_expr('dx^2', dx_squared)

# find the initial energy
E = simplify(Rational(1, 2) * (m * dx**2 + k * x**2))
print_expr('E', E)
E_0 = simplify(E.subs(t, 0))
print_expr('E_0', E_0)
#print(latex(E_0))

# find the change in energy over one cycle
# I get rid of the exponential term since it is basically identically one over the range of
# integration, and the integral is crazy if we keep it in.
delta_E = simplify(gamma * integrate(dx**2, (t, 0, 2 * pi / omega_1), conds='none'))
print_expr('delta_E (exact)', delta_E)
dx = simplify(exp(alpha * t) * dx)
delta_E = simplify(gamma * integrate(dx**2, (t, 0, 2 * pi / omega_1), conds='none'))
print_expr('delta_E (approx)', delta_E)

# Finally we combine them. To get things to simplify, we assume alpha is negligible, and omega_1 is
# basically equal to omega_0.
Q = simplify(2 * pi * E_0 / delta_E)
print_expr('Q', Q)
#print(latex(Q))
Q = simplify(Q.subs([(alpha, 0), (omega_1, omega_0)]))
print_expr('Q (alpha ~ 0, omega_1 ~ omega_0)', Q)
