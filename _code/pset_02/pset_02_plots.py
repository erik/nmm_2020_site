import numpy as np
import matplotlib.pyplot as plt

n_points = 1000

def p1_3_c():
    m = 1
    k = 1
    gamma = 0.1

    w_min = 0
    w_max = 2
    w = np.linspace(w_min, w_max, n_points)
    A = 1 / (-w**2 * m + w * 1j * gamma + k)

    fig1 = plt.figure()
    left, bottom, width, height = 0.1, 0.1, 0.8, 0.8
    ax1 = fig1.add_axes([left, bottom, width, height])
    ax1.plot(w, np.absolute(A))
    ax1.set_xlabel('ω')
    ax1.set_ylabel("magnitude")
    fig1.savefig("../../../assets/img/02_amplitude.png", transparent=True)

    fig2 = plt.figure()
    left, bottom, width, height = 0.1, 0.1, 0.8, 0.8
    ax2 = fig2.add_axes([left, bottom, width, height])
    ax2.plot(w, np.angle(A))
    ax2.set_xlabel('ω')
    ax2.set_ylabel("phase")
    fig2.savefig("../../../assets/img/02_phase.png", transparent=True)

def p1_3_f():
    t_min = 0
    t_max = 12 * np.pi
    t = np.linspace(t_min, t_max, n_points)
    w = 1.5
    eps = 0.5
    x_0 = np.exp(1j * w * t)
    x_1 = (
        (1 - eps / (3 * w**3) - eps / (6 * w**2)) * np.exp(1j * w * t) +
        (eps / (3 * w**3) - eps / (6 * w**2)) * np.exp(-1j * w * t) +
        (eps / (3 * w**2)) * np.exp(1j * 2 * t)
    )
    fig3 = plt.figure(figsize=[7.4, 4.8])
    left, bottom, width, height = 0.15, 0.1, 0.8, 0.9
    ax3 = fig3.add_axes([left, bottom, width, height])
    ax3.plot(t, np.real(x_0), label="x_0")
    ax3.plot(t, np.real(x_1), label="x_0 + ε x_1")
    ax3.set_xlabel('t')
    ax3.set_ylabel("real part")
    ax3.legend()
    fig3.savefig("../../../assets/img/02_perturbed_oscillator.png", transparent=True)

p1_3_c()
p1_3_f()
