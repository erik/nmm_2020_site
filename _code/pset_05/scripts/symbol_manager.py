from multi_indices import *
from sympy import *
from sympy.assumptions.assume import global_assumptions

class SymbolManager:
    def __init__(self):
        self.symbols = {}
        self.generic_symbols_dict = {}

    # Returns a positive integer 3, returns a list of strings ["x_0", "x_1", "x_2"].
    def vars(self, n, prefix = "x"):
        return [f"{prefix}_{{{i}}}" for i in range(0, n)]

    # Given a string "v", returns a SymPy symbol "v".
    def generic_symbol(self, name):
        if name not in self.generic_symbols_dict:
            self.generic_symbols_dict[name] = symbols(name, real=True)
        return self.generic_symbols_dict[name]

    # Given a list of strings ["v", "D"], returns a list of SymPy symbols ["v", "D"].
    def generic_symbols(self, names):
        return [self.generic_symbol(name) for name in names]

    # Given a positive integer 3, returns a list of SymPy symbols ["a_0", "a_1", "a_2"].
    def coefficients(self, n):
        return [symbols("a_" + str(i), real=True) for i in range(0, n)]

    # Given a string "x", returns a string "dx".
    @staticmethod
    def differential_name(var):
        return "d" + var

    # Given a string "x", returns a sympy symbol "dx".
    def differential_symbol(self, var):
        name = self.differential_name(var)
        if name not in self.symbols:
            self.symbols[name] = symbols(name, real=True, positive=True)
        return self.symbols[name]

    # Given a list of strings ["x", "y"], returns a list of sympy symbols ["dx", "dy"].
    def differential_symbols(self, vars):
        return [self.differential_symbol(var) for var in vars]

    # Given a list of strings ["x", "y"] and a multi-index (1, 2), returns a string "f^{(1x;2y)}".
    # Note that we have to use a semicolon rather than a comma, otherwise when we feed the name to
    # SymPy it thinks we're trying to create multiple symbols at once.
    @staticmethod
    def multi_derivative_name(multi_index):
        terms = [ str(index) for index in multi_index ]
        return "f^{(" + ";".join(terms) + ")}"

    # Given a list of strings ["x", "y"] and a multi-index (1, 2), returns a SymPy symbol
    # "f^{(1x;2y)}".
    def multi_derivative_symbol(self, multi_index):
        name = self.multi_derivative_name(multi_index)
        if name not in self.symbols:
            self.symbols[name] = symbols(name, real=True)
        return self.symbols[name]

    # Given a list of strings ["x", "y"] and a tuple of indices (1, 2), returns a string "f_{(1x;2y)}".
    # Note that we have to use a semicolon rather than a comma, otherwise when we feed the name to
    # SymPy it thinks we're trying to create multiple symbols at once.
    @staticmethod
    def sample_name(indices):
        terms = [ str(index) for index in indices ]
        return "f_{(" + ";".join(terms) + ")}"

    # Given a list of strings ["x", "y"] and a tuple of indices (1, 2), returns a SymPy symbol
    # "f_{(1x;2y)}".
    def sample_symbol(self, indices):
        name = self.sample_name(indices)
        if name not in self.symbols:
            self.symbols[name] = symbols(name, real=True)
        return self.symbols[name]
