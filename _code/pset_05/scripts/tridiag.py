from sympy import *
from sympy.assumptions.assume import global_assumptions

# This is a quick script to validate my solution for the tridiagonal system that comes up for
# implicit diffusion.

alpha = symbols("alpha", real=True, positive=True)

a = [None, -alpha, -alpha, -alpha, 0]
b = [1, 2 * alpha + 1, 2 * alpha + 1, 2 * alpha + 1, 1]
c = [0, -alpha, -alpha, -alpha, None]

c_prime = [None] * 5
c_prime[0] = (c[0] / b[0])
for i in range(1, 4):
    c_prime[i] = simplify(c[i] / (b[i] - a[i] * c_prime[i - 1]))
    # same thing
    #c_prime[i] = simplify(-alpha / (2 * alpha + 1 + alpha * c_prime[i - 1]))

print(latex(c_prime))
