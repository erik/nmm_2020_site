import math

# Need this below. Could use scipy's but right now I don't want to add the dependency.
def binomial_coefficient(n, k):
    assert(n >= 0)
    assert(k >= 0)
    if n < k:
        return 0
    if n == k or n == 0 or k == 0:
        return 1
    numerator = n
    x = n - 1
    while x > n - k:
        numerator *= x
        x -= 1
    return int(numerator / math.factorial(k))

# Generator for all n-tuples of non-negative integers that sum to k.
# It works by induction on n. If n == 1, then for any k the only possible multi-index is (k).
# Now assume we can compute all (n - 1)-tuples that sum to k. To get all n-tuples, iterate over
# values 0 <= i <= k, and yield (i,) + each (n - 1)-tuple that sums to k - i.
def multi_indices(n, k):
    assert(n > 0)
    assert(k >= 0)
    if n == 1:
        yield (k,)
    else:
        for i in range(0, k + 1):
            for sub_index in multi_indices(n - 1, k - i):
                yield (i,) + sub_index

# Returns the number of n-tuples of non-negative integers that sum to k.
def n_multi_indices(n, k):
    assert(n > 0)
    assert(k >= 0)
    return binomial_coefficient(n + k - 1, k)

def multi_index_order(mi):
    return sum((i for i in mi))

def multi_index_max(mi):
    return max(mi)

def multi_index_factorial(mi):
    # Note: in Python 3.8 and later, can use math.prod((factorial(i) for i in mi))
    result = 1
    for i in mi:
        result *= math.factorial(i)
    return result

def multi_index_power(vector, mi):
    result = 1
    for x, i in zip(vector, mi):
        result *= x**i
    return result
