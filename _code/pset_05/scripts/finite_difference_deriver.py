from multi_indices import *
import itertools
from symbol_manager import SymbolManager
from sympy import *
from sympy.assumptions.assume import global_assumptions


# taylor series up to order n of f(x_0 + index * dx)
def taylor_series(syms, var, index, n):
    return multi_taylor_series(syms, [var], [index], n)

# Expands f(vars[0] + indices[0] * dvar0, vars[1] + indices[1] * dvar1, ...) to the specified order.
# - syms is a SymbolManager
# - vars is a list of strings e.g. ["x", "t"]
# - indices is a list of scalars, e.g. [1, 1]
# - order is a scalar e.g. 3
def multi_taylor_series(syms, vars, indices, order):
    assert(len(vars) == len(indices))
    n = len(vars)
    differentials = syms.differential_symbols(vars)
    result = 0
    for i in range(0, order + 1):
        for multi_index in multi_indices(n, i):
            numerator = multi_index_power(indices, multi_index)
            denominator = multi_index_factorial(multi_index)
            dx = multi_index_power(differentials, multi_index)
            df = syms.multi_derivative_symbol(multi_index)
            result += numerator * dx * df / denominator
    return result

def extract_terms_with(sympy_expression, target):
    expanded_expression = sympy_expression.expand()
    if expanded_expression.func == Add:
        return Add(*[argi for argi in sympy_expression.expand().args if argi.has(target)])
    else:
        return expanded_expression if expanded_expression.has(target) else 0

def extract_powers_of(sympy_expression, targets):
    expanded_expression = sympy_expression.expand()
    result = [0] * len(targets)
    # Note: from my own testing I believe that symbols are never put inside Rational instances.
    # Specifically, x/2 becomes Mul(Rational(1, 2), x), and 2/x becomes Mul(2, Pow(x, -1)).
    # If this isn't true then this branch is broken.
    if isinstance(sympy_expression, Integer) or isinstance(sympy_expression, Rational):
        pass
    elif isinstance(sympy_expression, Symbol):
        if sympy_expression in targets:
            index = targets.index(sympy_expression)
            result[index] = 1
    elif isinstance(sympy_expression, Pow):
        result = extract_powers_of(sympy_expression.args[0], targets)
        result = [r * sympy_expression.args[1] for r in result]
    elif isinstance(sympy_expression, Mul):
        for arg in sympy_expression.args:
            tmp_result = extract_powers_of(arg, targets)
            for i in range(0, len(targets)):
                result[i] += tmp_result[i]
    else:
        print(srepr(sympy_expression))
        print(sympy_expression.func)
        raise NotImplementedError
    return result

class FDApproximation:
    def __init__(self, samples, coefficients, constraints):
        assert(len(samples) == len(coefficients) > 0)
        arity = len(samples[0])
        for sample in samples[1:]:
            assert(len(sample) == arity)

        self.arity = arity
        self.samples = samples
        self.coefficients = coefficients
        self.constraints = {cstrt[0] : cstrt[1] for cstrt in constraints}

    def expansion(self, syms, n):
        vars = syms.vars(self.arity)
        result = 0
        for sample, coeff in zip(self.samples, self.coefficients):
            result += coeff * multi_taylor_series(syms, vars, sample, n)
        return result.expand()

    # Finds the order of each variable and the leading error terms.
    # Note that it just searches until it finds some error terms. It usually finds the lowest order
    # terms first, but that's not guaranteed.
    def orders_and_leading_errors(self, syms, max_terms = 100):
        # constants
        vars = syms.vars(self.arity)
        differentials = syms.differential_symbols(vars)

        # return values
        orders = [0] * self.arity
        error_terms = [0] * self.arity
        leading_error = 0

        # iteration vars
        i = 0
        done = False
        terms_considered = 0

        while done is False:
            for alpha in multi_indices(self.arity, i):
                # Whether it's zero or not, it's what we wanted so doesn't count as error.
                if alpha in self.constraints:
                    continue

                # Figure out what the coefficient is for the alpha term.
                coefficient = 0
                for cf, sample in zip(self.coefficients, self.samples):
                    coefficient += cf * multi_index_power(sample, alpha)
                if coefficient == 0:
                    continue

                # Extract powers of each var's differential distance, and compute the error term.
                coefficient *= multi_index_power(differentials, alpha)
                new_orders = extract_powers_of(coefficient, differentials)
                error_term = coefficient * syms.multi_derivative_symbol(alpha) / multi_index_factorial(alpha)

                # Save results and determine if there are any vars that haven't come up yet.
                leading_error += error_term
                found_all_vars = True
                for j in range(0, self.arity):
                    if new_orders[j] > 0 and (orders[j] == 0 or new_orders[j] < orders[j]):
                        orders[j] = new_orders[j]
                        error_terms[j] = error_term
                    elif orders[j] == 0:
                        found_all_vars = False
                if found_all_vars:
                    done = True

                # Check failsafe termination condition.
                terms_considered += 1
                if (terms_considered >= max_terms):
                    print("max terms reached")
                    done = True

                if done:
                    break

            i += 1

        return orders, error_terms, leading_error

    def apply(self, syms, indices = None):
        if indices is None:
            indices = [0] * self.arity

        result = 0
        for sample, coefficient in zip(self.samples, self.coefficients):
            new_indices = [indices[i] + sample[i] for i in range(0, self.arity)]
            result += coefficient * syms.sample_symbol(new_indices)
        return result

    def von_neumann_substitution(self, syms):
        # We assume the last dimension is time, and the others space.
        vars = syms.vars(self.arity)
        differentials = syms.differential_symbols(vars)
        A = syms.generic_symbol("A")
        n = syms.generic_symbol("n")
        j = syms.generic_symbols(syms.vars(self.arity - 1, "j"))
        k = syms.generic_symbols(syms.vars(self.arity - 1, "k"))

        expr = self.apply(syms)
        substitutions = []
        for sample in self.samples:
            old_sample = syms.sample_symbol(sample)
            wave_sum = sum([ki * (ji + si) * dx for ki, ji, si, dx in zip(k, j, sample[0:-1], differentials[0:-1])])
            vn_expr = A**(n + sample[-1]) * exp(I * wave_sum)
            substitutions.append((old_sample, vn_expr))

        expr = expr.subs(substitutions)
        for i in range(0, self.arity - 1):
            expr /= exp(I * j[i] * k[i] * differentials[i])
        expr /= A**n
        return expr.expand()

# Derives a finite difference expression for the mixed partial derivative of f specified by
# multi_index, given evalations of f at the specified samples. Samples should be of the form
# [sample_1, sample_2, ...] where each sample is itself a list [i0 * dvar0, i1 * dvar1, ...].
# - syms is a SymbolManager
# - vars is a list of variable names ["x", "t"]
# - samples is a list of sample locations [[0, 0], [1, 0]]
# - Constraints is a list of tuples of multi-index tuples and sympy symbol values, e.g.
#   [((0, 0), 0), ((1, 0), 1)]. The applied constraints are
#   sum_{n=1}^d a_n s_n^alpha = value * alpha! / dx_n^alpha
#   where d is the number of samples, a_n are the undetermined coefficients, s_n are the samples,
#   alpha is the multi-index, and value the value.
def derive_multi_approximation(syms, samples, constraints):
    n_coeffs = len(samples)
    arity = len(samples[0])

    # basic sanity checks
    assert(n_coeffs > 0)
    for sample in samples[1:]:
        assert(len(sample) == arity) # should be a sample
    assert(len(constraints) == n_coeffs) # want a square system (hopefully uniquely determined)
    for constraint in constraints:
        assert(len(constraint) == 2) # should be a pair
        assert(len(constraint[0]) == arity) # should be a multi-index

    # declare variables
    vars = syms.vars(arity)
    differentials = syms.differential_symbols(vars)

    # define system of linear equations m * a = b
    m = []
    b = []
    for constraint in constraints:
        alpha = constraint[0]
        value = constraint[1]
        m.append([multi_index_power(sample, alpha) for sample in samples])
        b.append(value * multi_index_factorial(alpha) / multi_index_power(differentials, alpha))

    # ensure m isn't singular
    m = Matrix(m)
    b = Matrix(b)
    if m.det() == 0:
        print("Error: constraint matrix is singular")
        print(latex(m))
        return None

    # solve
    coeffs = m.LUsolve(b)
    return FDApproximation(samples, coeffs, constraints)

def print_difference(sm, name, vars, samples, constraints):
    print(name)
    approx = derive_multi_approximation(sm, samples, constraints)
    if approx is None:
        print("")
        return
    # todo sub vars
    print("Coefficients: " + latex(approx.coefficients))
    print("Approximation: " + latex(approx.apply(sm)))
    orders, error_terms, leading_error = approx.orders_and_leading_errors(sm)
    print(f"Orders: {orders}")
    print(f"Leading error terms: {latex(leading_error)}")
    print("Leading error by var:")
    for var, order, term in zip(vars, orders, error_terms):
        #print("  " + latex(var) + " : " + latex(term))
        print(f"  {var} (order {order}): {latex(term)}")
    print("")
    return approx

if __name__ == "__main__":
    sm = SymbolManager()

    # approximations for df/dx
    print_difference(sm, "Forward df/dx", ["x"], [[0], [1]], [((0,), 0), ((1,), 1)])
    print_difference(sm, "Backward df/dx", ["x"], [[-1], [0]], [((0,), 0), ((1,), 1)])
    print_difference(sm, "Central df/dx", ["x"], [[-1], [0], [1]], [((0,), 0), ((1,), 1), ((2,), 0)])

    # approximations for df^2/dx^2
    print_difference(sm, "Central d^2f/dx^2", ["x"], [[-1], [0], [1]], [((0,), 0), ((1,), 0), ((2,), 1)])
    print_difference(sm, "Fourth order d^2f/dx^2", ["x"],
        [[-2], [-1], [0], [1], [2]],
        [((0,), 0), ((1,), 0), ((2,), 1), ((3,), 0), ((4,), 0)]
    )

    # approximations for df^2/(dx dt)
    print_difference(sm, "Forward/backward d^2f/(dx dt)", ["x", "t"],
        [
            [0,  0], [1, 0],
            [0, -1], [1, -1]
        ],
        [((0, 0), 0), ((1, 0), 0), ((0, 1), 0), ((1, 1), 1)]
    )
    print_difference(sm, "Central/backward d^2f/(dx dt)", ["x", "t"],
        [
            [-1,  0], [0,  0], [1,  0],
            [-1, -1], [0, -1], [1, -1]
        ],
        [
            ((0, 0), 0),
            ((0, 1), 0), ((1, 0), 0),
            # ((0, 2), 0) is dependent on ((0, 1), 0)
            ((1, 1), 1), ((2, 0), 0),
            # ((0, 3), 0) is dependent on ((0, 1), 0)
            # ((3, 0), 0) is dependent on ((1, 0), 0)
            # ((1, 2), 0) is dependent on ((1, 1), 0)
            ((2, 1), 0)
        ]
    )
    print_difference(sm, "Central/central d^2f/(dx dt)", ["x", "t"],
        [
            [-1,  1], [0,  1], [1,  1],
            [-1,  0], [0,  0], [1,  0],
            [-1, -1], [0, -1], [1, -1],
        ],
        [
            ((0, 0), 0),
            ((0, 1), 0), ((1, 0), 0),
            ((1, 1), 1), ((2, 0), 0), ((0, 2), 0),
            # The constraint ((0, 3), 0) is dependent on ((0, 1), 0)
            # The constraint ((3, 0), 0) is dependent on ((1, 0), 0)
            ((2, 1), 0), ((1, 2), 0),
            # The constraints ((3, 1), 0), ((3, 1), 0), ((0, 4), 0), and ((4, 0), 0) are dependent
            ((2, 2), 0)
        ]
    )

    # approximations for df^3/(dx^2 dt)
    print_difference(sm, "Central/backward d^3f/(dx^2 dt)", ["x", "t"],
        [
            [-1,  0], [0,  0], [1,  0],
            [-1, -1], [0, -1], [1, -1]
        ],
        [
            ((0, 0), 0),
            ((0, 1), 0), ((1, 0), 0),
            # ((0, 2), 0) is dependent on ((0, 1), 0)
            ((1, 1), 0), ((2, 0), 0),
            ((2, 1), 1)
        ]
    )
    print_difference(sm, "Central/central d^3f/(dx^2 dt)", ["x", "t"],
        [
            [-1,  1], [0,  1], [1,  1],
            [-1,  0], [0,  0], [1,  0],
            [-1, -1], [0, -1], [1, -1],
        ],
        [
            ((0, 0), 0),
            ((0, 1), 0), ((1, 0), 0),
            ((1, 1), 0), ((2, 0), 0), ((0, 2), 0),
            # The constraint ((0, 3), 0) is dependent on ((0, 1), 0)
            # The constraint ((3, 0), 0) is dependent on ((1, 0), 0)
            ((2, 1), 1), ((1, 2), 0),
            # The constraints ((3, 1), 0), ((3, 1), 0), ((0, 4), 0), and ((4, 0), 0) are dependent
            ((2, 2), 0)
        ]
    )
