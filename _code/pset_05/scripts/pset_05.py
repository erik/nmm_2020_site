from finite_difference_deriver import *


def print_expr(name, expr):
    print(name + ": ")
    pprint(expr)
    print("")


# Prints a fd in x and t in a prettier format.
# In particular it replaces f(x_0 + i dx_0, x_1 + j dx_1) with u_i^j, dx_0 with dx, and dx_1 with
# dt.
def pretty_form(syms, fd):
    arity = 2
    vars = syms.vars(arity)

    substitutions = []
    for sample in fd.samples:
        old_sample = syms.sample_symbol(sample)
        new_sample = f"u_{{{sample[0]}}}^{{{sample[1]}}}"
        new_sample = syms.generic_symbol(new_sample)
        substitutions.append((old_sample, new_sample))

    substitutions.append((syms.differential_symbol(vars[0]), syms.differential_symbol("x")))
    substitutions.append((syms.differential_symbol(vars[1]), syms.differential_symbol("t")))

    expr = fd.apply(syms)
    return expr.subs(substitutions)


def problem_8_1():
    sm = SymbolManager()
    v = symbols('v', real=True)

    print("================================================================================")
    # (a)
    approx = print_difference(sm, "8.1 (a): central d^2f/dt^2 - v^2 d^2f/dx^2", ["x", "t"],
        [
                      [0,  1],
            [-1,  0], [0,  0], [1,  0],
                      [0, -1],
        ],
        [
            ((0, 0), 0),
            ((1, 0), 0), ((0, 1), 0),
            ((2, 0), -v**2), ((0, 2), 1),
        ]
    )
    vn_expr = approx.von_neumann_substitution(sm)
    print(latex(vn_expr))
    #sols = solve(vn_expr, A)
    #print(latex(sols))
    #print(latex(simplify(sols[0])))
    return


    print("================================================================================")
    # Alternate way to derive (a). Shows that we don't gain anything by including corners.
    print_difference(sm, "8.1 (a) full square d^2f/dt^2 - v^2 d^2f/dx^2", ["x", "t"],
        [
            [-1,  1], [0,  1], [1,  1],
            [-1,  0], [0,  0], [1,  0],
            [-1, -1], [0, -1], [1, -1],
        ],
        [
            ((0, 0), 0),
            ((1, 0), 0), ((0, 1), 0),
            ((2, 0), -v**2), ((0, 2), 1), ((1, 1), 0),
            # (3, 0), (0, 3), (3, 1) already zero
            ((1, 2), 0), ((2, 1), 0),
            ((2, 2), 0),
        ]
    )

    print("================================================================================")
    gamma = symbols('gamma', real=True)
    method = print_difference(sm, "8.1 (h) house d^2/dt^2 f - v^2 d^2/dx^2 f - gamma d/dt d^2/dx^2 f", ["x", "t"],
        [
                      [0,  1],
            [-1,  0], [0,  0], [1,  0],
            [-1, -1], [0, -1], [1, -1],
        ],
        [
            ((0, 0), 0),
            ((1, 0), 0), ((0, 1), 0),
            ((2, 0), -v**2), ((0, 2), 1), ((1, 1), 0),
            ((2, 1), -gamma),
        ]
    )
    expr = pretty_form(sm, method)
    print(latex(expr))
    new_sample = "u_{0}^{1}"
    new_sample = sm.generic_symbol(new_sample)
    solution = solve(expr, new_sample)
    print(latex(simplify(solution[0])))
    print("")

    print("================================================================================")
    implicit_method = print_difference(sm, "8.1 (h) full square d^2/dt^2 f - v^2 d^2/dx^2 f - gamma d/dt d^2/dx^2 f", ["x", "t"],
        [
            [-1,  1], [0,  1], [1,  1],
            [-1,  0], [0,  0], [1,  0],
            [-1, -1], [0, -1], [1, -1],
        ],
        [
            ((0, 0), 0),
            ((1, 0), 0), ((0, 1), 0),
            ((2, 0), -v**2), ((0, 2), 1), ((1, 1), 0),
            # (3, 0), (0, 3), (3, 1) already zero
            ((1, 2), 0), ((2, 1), -gamma),
            ((2, 2), 0),
        ]
    )
    expr = pretty_form(sm, implicit_method)
    dt = sm.differential_symbol("t")
    print(latex(simplify(dt**2 * expr)))


def problem_8_2():
    sm = SymbolManager()
    D = symbols('D', real=True)

    print("================================================================================")
    method = print_difference(sm, "8.2 (a) explicit 1D order (2, 1) d/dt u - D d^2/dx^2 u", ["x", "t"],
        [
                      [0,  1],
            [-1,  0], [0,  0], [1,  0],
        ],
        [
            ((0, 0), 0),
            ((1, 0), 0), ((0, 1), 1),
            ((2, 0), -D)
        ]
    )
    expr = pretty_form(sm, method)
    print(latex(expr))
    new_sample = "u_{0}^{1}"
    new_sample = sm.generic_symbol(new_sample)
    solution = solve(expr, new_sample)
    print(latex(simplify(solution[0])))
    print("")

    vn_expr = method.von_neumann_substitution(sm)
    print(latex(vn_expr))
    A = sm.generic_symbol("A")
    sols = solve(vn_expr, A)
    print(latex(simplify(sols[0])))
    print("")


    print("================================================================================")
    print("inherently unstable")
    method = print_difference(sm, "8.2 (a) explicit 1D order (2, 2) d/dt u - D d^2/dx^2 u", ["x", "t"],
        [
                      [0,  1],
            [-1,  0], [0,  0], [1,  0],
                      [0, -1],
        ],
        [
            ((0, 0), 0),
            ((1, 0), 0), ((0, 1), 1),
            ((2, 0), -D), ((0, 2), 0)
        ]
    )
    expr = pretty_form(sm, method)
    print(latex(expr))
    new_sample = "u_{0}^{1}"
    new_sample = sm.generic_symbol(new_sample)
    solution = solve(expr, new_sample)
    print(latex(simplify(solution[0])))
    print("")

    vn_expr = method.von_neumann_substitution(sm)
    print(latex(vn_expr))
    print("")


    print("================================================================================")
    method = print_difference(sm, "8.2 (b) implicit 1D order (2, 1) d/dt u - D d^2/dx^2 u", ["x", "t"],
        [
            [-1,  1], [0,  1], [1,  1],
                      [0,  0],
        ],
        [
            ((0, 0), 0),
            ((1, 0), 0), ((0, 1), 1),
            # ((0, 2), ?) is implied by ((0, 1), 0)
            ((2, 0), -D),
        ]
    )
    expr = pretty_form(sm, method)
    print(latex(expr))
    new_sample = "u_{0}^{1}"
    new_sample = sm.generic_symbol(new_sample)
    solution = solve(expr, new_sample)
    print(latex(simplify(solution[0])))
    print("")

    vn_expr = method.von_neumann_substitution(sm)
    print(latex(vn_expr))
    A = sm.generic_symbol("A")
    sols = solve(vn_expr, A)
    print(latex(simplify(sols[0])))
    print("")


    print("================================================================================")
    print("Crank Nicholson")
    vars = sm.vars(2)
    differentials = sm.differential_symbols(vars)
    dx = differentials[0]
    dt = differentials[1]
    method = print_difference(sm, "8.2 (b) implicit 1D order (2, 1) d/dt u - D d^2/dx^2 u", ["x", "t"],
        [
            [-1,  1], [0,  1], [1,  1],
            [-1,  0], [0,  0], [1,  0],
        ],
        [
            ((0, 0), 0),
            ((1, 0), 0), ((0, 1), 1),
            # ((0, 2), ?) is implied by ((0, 1), 1)
            ((2, 0), -D), ((1, 1), 0),
            # ((0, 3), ?) is implied by ((0, 1), 1)
            # ((1, 2), 0) is implied by ((1, 1), 0)
            # ((3, 0), 0) is implied by ((1, 0), 0)
            # This cancels out the error in (0, 2)
            ((2, 1), -D * dt/2)
        ]
    )
    print("Expansion: " + latex(method.expansion(sm, 4)))
    print("")
    expr = pretty_form(sm, method)
    print(latex(expr))
    new_sample = "u_{0}^{1}"
    new_sample = sm.generic_symbol(new_sample)
    solution = solve(expr, new_sample)
    print(latex(simplify(solution[0])))
    print("")


    print("================================================================================")
    print("this also fails to change the order in time")
    method = print_difference(sm, "8.2 (b) implicit 1D order (2, 1) d/dt u - D d^2/dx^2 u", ["x", "t"],
        [
            [-1,  1], [0,  1], [1,  1],
                      [0,  0],
                      [0, -1],
        ],
        [
            ((0, 0), 0),
            ((1, 0), 0), ((0, 1), 1),
            ((0, 2), 0),
            # ((0, 2), ?) is implied by ((0, 1), 0)
            ((2, 0), -D),
        ]
    )
    expr = pretty_form(sm, method)
    print(latex(expr))
    new_sample = "u_{0}^{1}"
    new_sample = sm.generic_symbol(new_sample)
    solution = solve(expr, new_sample)
    print(latex(simplify(solution[0])))
    print("")

    vn_expr = method.von_neumann_substitution(sm)
    print(latex(vn_expr))
    A = sm.generic_symbol("A")
    sols = solve(vn_expr, A)
    print(latex(simplify(sols[0])))
    print("")


    """
    print("================================================================================")
    print("reduces to second (unstable order 2,2 explicit)")
    method = print_difference(sm, "8.2 (b) implicit 1D order (2, 2) d/dt u - D d^2/dx^2 u", ["x", "t"],
        [
            [-1,  1], [0,  1], [1,  1],
            [-1,  0], [0,  0], [1,  0],
                      [0, -1],
        ],
        [
            ((0, 0), 0),
            ((1, 0), 0), ((0, 1), 1),
            ((2, 0), -D), ((1, 1), 0), ((0, 2), 0),
            # (0, 3) fixed
            # (3, 0) fixed
            # (1, 2) fixed
            ((2, 1), 0),
        ]
    )
    expr = pretty_form(sm, method)
    print(latex(expr))
    new_sample = "u_{0}^{1}"
    new_sample = sm.generic_symbol(new_sample)
    solution = solve(expr, new_sample)
    print(latex(simplify(solution[0])))
    print("")
    """


    """
    print("================================================================================")
    print("also reduces to second (unstable order 2,2 explicit)")
    method = print_difference(sm, "8.2 (b) implicit 1D order (2, 2) d/dt u - D d^2/dx^2 u", ["x", "t"],
        [
            [-1,  1], [0,  1], [1,  1],
            [-1,  0], [0,  0], [1,  0],
            [-1, -1], [0, -1], [1, -1],
        ],
        [
            ((0, 0), 0),
            ((1, 0), 0), ((0, 1), 1),
            ((2, 0), -D), ((1, 1), 0), ((0, 2), 0),
            # (0, 3) fixed by (0, 1)
            # (3, 0) fixed nonzero by (1, 0)
            ((1, 2), 0), ((2, 1), 0),
            ((2, 2), 0)
        ]
    )
    expr = pretty_form(sm, method)
    print(latex(expr))
    new_sample = "u_{0}^{1}"
    new_sample = sm.generic_symbol(new_sample)
    solution = solve(expr, new_sample)
    print(latex(simplify(solution[0])))
    print("")
    """

if __name__ == "__main__":
    #problem_8_1()
    problem_8_2()
