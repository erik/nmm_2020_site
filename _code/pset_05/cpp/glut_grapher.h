#ifndef NMM_GLUT_GRAPHER_H
#define NMM_GLUT_GRAPHER_H

#include "scalar.h"
#include "vector.h"
#include "bounding_box.h"
#include <GL/glut.h>

//--------------------------------------------------------------------------------------------------
struct SimulationInterface {
    virtual void step() = 0;
    virtual VectorX<Scalar> const& state() const = 0;
};

//--------------------------------------------------------------------------------------------------
class GlutGrapher {
public:
    GlutGrapher() = default;
    void initialize(
        SimulationInterface* sim,
        BoundingBox<Vector2<Scalar>> const& bounds
    );

    void init_glut();
    void run() { glutMainLoop(); }
    void display() const;
    void idle();

    Vector2<uint32_t> const& window_size() const { return window_size_; }
    Scalar aspect_ratio() const { return aspect_ratio_; }

public:
    static constexpr uint32_t max_width_ = 1200;
    static constexpr uint32_t max_height_ = 900;

    Vector2<uint32_t> window_size_;
    Scalar aspect_ratio_;
    Vector2<Scalar> translation_;
    Scalar zoom_;

    uint32_t frame_;
    SimulationInterface* sim_;
    Scalar d_;
};

#endif
