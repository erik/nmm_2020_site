#include <iostream>
#include "clara.hpp"
#include "glut_grapher.h"
#include "scalar.h"
#include "vector.h"

//--------------------------------------------------------------------------------------------------
GlutGrapher grapher;
void display() { grapher.display(); }
void idle() { grapher.idle(); }
void mouse(int, int, int, int) { exit(0); }

//--------------------------------------------------------------------------------------------------
class WaveSim : public SimulationInterface {
public:
    WaveSim();
    void initialize(bool use_damping, Scalar ampd, Scalar amp1, Scalar amp2, Scalar amp3, Scalar amp4, Scalar amp5);
    void step() final;
    VectorX<Scalar> const& state() const final { return state_n; }

    Scalar dt() const { return delta_t; }
    Scalar dx() const { return delta_x; }
    Scalar amp() const { return amplitude; }

private:
    static constexpr uint32_t n = 1000;
    //constexpr uint32_t n2 = n + 2;
    Scalar const v = 1;
    Scalar const gamma = 0.01;
    Scalar const delta_t = 0.2;
    Scalar const delta_x = 1;
    Scalar const a1 = v * v * delta_t * delta_t / (delta_x * delta_x);
    Scalar const a2 = 2 * (1 - a1);
    Scalar const a3 = gamma * delta_t / (delta_x * delta_x);
    Scalar const amplitude = n * delta_x / 2;

    Scalar const boundary_0 = 0;
    Scalar const boundary_1 = 0;

    VectorX<Scalar> state_next_n;
    VectorX<Scalar> state_n;
    VectorX<Scalar> state_prev_n;
    bool damping;
};

//..................................................................................................
WaveSim::WaveSim() {
    state_next_n.resize(n);
    state_n.resize(n);
    state_prev_n.resize(n);
    state_next_n.setZero();
    state_n.setZero();
    state_prev_n.setZero();
}

//..................................................................................................
void WaveSim::initialize(bool use_damping, Scalar ampd, Scalar amp1, Scalar amp2, Scalar amp3, Scalar amp4, Scalar amp5) {
    damping = use_damping;
    state_n.setZero();

    // delta function
    state_n[n/2] += amplitude * ampd;

    // first mode
    for (uint32_t i = 0; i < n; ++i) {
        state_n[i] += amplitude * amp1 * std::sin(i * 3.14159265359 / n);
    }

    // second mode
    for (uint32_t i = 0; i < n; ++i) {
        state_n[i] += amplitude * amp2 * std::sin(i * 2 * 3.14159265359 / n);
    }

    // third mode
    for (uint32_t i = 0; i < n; ++i) {
        state_n[i] += amplitude * amp3 * std::sin(i * 3 * 3.14159265359 / n);
    }

    // fourth mode
    for (uint32_t i = 0; i < n; ++i) {
        state_n[i] += amplitude * amp4 * std::sin(i * 4 * 3.14159265359 / n);
    }

    // fifth mode
    for (uint32_t i = 0; i < n; ++i) {
        state_n[i] += amplitude * amp5 * std::sin(i * 5 * 3.14159265359 / n);
    }

    // ensure boundary conditions are met
    state_n[0] = boundary_0;
    state_n[n - 1] = boundary_1;
    state_prev_n = state_n;
}

//..................................................................................................
void WaveSim::step() {
    for (uint32_t i = 1; i < n - 1; ++i) {
        state_next_n[i] = a2 * state_n[i] + a1 * (state_n[i-1] + state_n[i+1]) - state_prev_n[i];
        if (damping) {
            state_next_n[i] += a3 * (
                (state_n[i-1] - 2 * state_n[i] + state_n[i+1]) -
                (state_prev_n[i-1] - 2 * state_prev_n[i] + state_prev_n[i+1])
            );
        }
    }
    // this should allow Eigen to use SIMD instructions
    //state_next_n.segment<n>(1) = -state_prev_n.segment<n>(1) + a2 * state_n.segment<n>(1)

    state_prev_n.swap(state_n);
    state_n.swap(state_next_n);
}

//--------------------------------------------------------------------------------------------------
int main(int argc, char **argv) {
    // Define CLI interface.
    bool no_damping = false;
    Scalar ampd = 0;
    Scalar amp1 = 0;
    Scalar amp2 = 0;
    Scalar amp3 = 0;
    Scalar amp4 = 0;
    Scalar amp5 = 0;
    auto const cli =
        clara::Opt(no_damping)["-n"]["--no-damping"]("disables the damping term") |
        clara::Opt(ampd, "ampd")["-z"]["--max-regions"]("amplitude of delta function") |
        clara::Opt(amp1, "amp1")["-a"]["--amp-1"]("amplitude of mode 1") |
        clara::Opt(amp2, "amp2")["-b"]["--amp-2"]("amplitude of mode 2") |
        clara::Opt(amp3, "amp3")["-c"]["--amp-3"]("amplitude of mode 3") |
        clara::Opt(amp4, "amp4")["-d"]["--amp-4"]("amplitude of mode 4") |
        clara::Opt(amp5, "amp5")["-e"]["--amp-5"]("amplitude of mode 5");

    // Parse arguments.
    auto const result = cli.parse(clara::Args(argc, argv));
    if (!result) {
        std::cerr << "Error in command line: " << result.errorMessage() << std::endl;
        exit(1);
    }

    WaveSim sim;
    sim.initialize(!no_damping, ampd, amp1, amp2, amp3, amp4, amp5);
    grapher.initialize(
        &sim,
        BoundingBox<Vector2<Scalar>>(
            {0, -1.2 * sim.amp()},
            {sim.dx() * sim.state().size(), 1.2 * sim.amp()}
        )
    );
    grapher.init_glut();
    glutDisplayFunc(display);
    glutMouseFunc(mouse);
    glutIdleFunc(idle);
    grapher.run();

    return 0;
}
