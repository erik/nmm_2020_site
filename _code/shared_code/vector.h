#ifndef NMM_VECTOR_H
#define NMM_VECTOR_H

#include <Eigen/Core>
#include <Eigen/Sparse>
#include <iostream>
#include <iomanip>

//--------------------------------------------------------------------------------------------------
template <typename T>
using Vector2 = Eigen::Matrix<T, 2, 1>;

//--------------------------------------------------------------------------------------------------
template <typename T>
using Vector3 = Eigen::Matrix<T, 3, 1>;

//--------------------------------------------------------------------------------------------------
template <typename T>
using VectorX = Eigen::Matrix<T, Eigen::Dynamic, 1>;

//--------------------------------------------------------------------------------------------------
template <typename T>
using Matrix2 = Eigen::Matrix<T, 2, 2>;

//--------------------------------------------------------------------------------------------------
template <typename T>
using Matrix3 = Eigen::Matrix<T, 3, 3>;

//--------------------------------------------------------------------------------------------------
template <typename T>
using MatrixX = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;

//--------------------------------------------------------------------------------------------------
template <typename T>
using MatrixX = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;

//--------------------------------------------------------------------------------------------------
template <typename T>
using SparseMatrix = Eigen::SparseMatrix<T>;

//--------------------------------------------------------------------------------------------------
template <typename T>
void print_vector(VectorX<T> const& vec) {
    std::cout << std::fixed << std::setprecision(3);
    for (uint32_t i = 0; i < vec.size() - 1; ++i) {
        std::cout << std::setw(5) << vec[i] << ", ";
    }
    std::cout << vec[vec.size() - 1] << '\n';
}

#endif
