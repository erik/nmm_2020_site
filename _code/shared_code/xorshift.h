#ifndef XORSHIFT_H
#define XORSHIFT_H

#include <stdint.h>

//--------------------------------------------------------------------------------------------------
// from Numerical Recipes in C, 3rd Edition
class XorShift64 {
public:
   XorShift64 (): x_(0xdefceed) {}
   XorShift64 (uint64_t seed): x_(seed) {}
   inline XorShift64& advance();
   inline uint64_t draw_uint64() { advance(); return x_; }
   inline uint32_t draw_uint32() { advance(); return static_cast<uint32_t>(x_); }
   inline double draw_double() { advance(); return 5.42101086242752217e-20 * x_; }

private:
   uint64_t x_;
   static constexpr uint32_t a1_ = 21;
   static constexpr uint32_t a2_ = 35;
   static constexpr uint32_t a3_ = 4;
};

//..................................................................................................
XorShift64& XorShift64::advance() {
   x_ = x_ ^ (x_ >> a1_);
   x_ = x_ ^ (x_ << a2_);
   x_ = x_ ^ (x_ >> a3_);
   return *this;
}

#endif
