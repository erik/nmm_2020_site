#ifndef NMM_BOUNDING_BOX_H
#define NMM_BOUNDING_BOX_H

#include <iostream>
#include <stdint.h>

//--------------------------------------------------------------------------------------------------
// An axis aligned bounding box. It stores a low corner and a high corner, each represented as a T.
// The idea is that this makes BoundingBox dimension and representation agnostic, though the class
// is designed with fixed size Eigen vectors in mind for the latter.
template <typename T>
class BoundingBox {
public:
    BoundingBox() = default;
    BoundingBox(BoundingBox const&) = default;
    BoundingBox(BoundingBox&&) = default;
    BoundingBox(T const& x): low_(x), high_(x) {}
    BoundingBox(T const& low, T const& high): low_(low), high_(high) {}

    BoundingBox& operator=(BoundingBox const&) = default;
    BoundingBox& operator=(BoundingBox&&) = default;
    BoundingBox& operator=(T const& x);

    T const& low() const { return low_; }
    T const& high() const { return high_; }
    T extents() const { return high_ - low_; }
    typename T::Scalar extent(uint32_t i) const { return high_[i] - low_[i]; }
    T center() const { return 0.5 * (low_ + high_); }

    bool empty() const;
    bool nonempty() const;
    bool contains(T const& x) const;
    bool contains(BoundingBox const& box) const;
    bool intersects(BoundingBox const& box) const;
    bool on_boundary(T const& x) const;

    BoundingBox expansion(T const& x) const;
    BoundingBox expansion(BoundingBox const& box) const;
    BoundingBox intersection(BoundingBox const& box) const;
    BoundingBox translation(T const& displacement) const;
    BoundingBox offset(typename T::Scalar distance) const;
    BoundingBox scaled(typename T::Scalar factor) const;

    BoundingBox& expand(T const& x);
    BoundingBox& expand(BoundingBox const& box);
    BoundingBox& intersect(BoundingBox const& box);
    BoundingBox& translate(T const& displacement);
    BoundingBox& apply_offset(typename T::Scalar distance);
    BoundingBox& scale(typename T::Scalar factor);

private:
    T low_;
    T high_;
};

//..................................................................................................
template <typename T>
BoundingBox<T>& BoundingBox<T>::operator=(T const& x) {
    low_ = x;
    high_ = x;
    return *this;
}

//..................................................................................................
template <typename T>
bool BoundingBox<T>::empty() const {
    return (low_.array() > high_.array()).any();
}

//..................................................................................................
template <typename T>
bool BoundingBox<T>::nonempty() const {
    return (low_.array() <= high_.array()).all();
}

//..................................................................................................
template <typename T>
bool BoundingBox<T>::contains(T const& x) const {
    return (low_.array() <= x.array()).all() && (x.array() <= high_.array()).all();
}

//..................................................................................................
template <typename T>
bool BoundingBox<T>::contains(BoundingBox const& box) const {
    return contains(box.low()) && contains(box.high());
}

//..................................................................................................
template <typename T>
bool BoundingBox<T>::intersects(BoundingBox const& box) const {
    // An equivalent implementation would be
    // return intersection(box).nonempty();
    // But this version permits more expression template magic.
    return (low_.cwiseMax(box.low()).array() <= high_.cwiseMin(box.high()).array()).all();
}

//..................................................................................................
template <typename T>
bool BoundingBox<T>::on_boundary(T const& x) const {
    return (low_.array() == x.array()).any() || (x.array() == high_.array()).any();
}

//..................................................................................................
template <typename T>
BoundingBox<T> BoundingBox<T>::expansion(T const& x) const {
    return BoundingBox(low_.cwiseMin(x), high_.cwiseMax(x));
}

//..................................................................................................
template <typename T>
BoundingBox<T> BoundingBox<T>::expansion(BoundingBox const& box) const {
    return BoundingBox(low_.cwiseMin(box.low()), high_.cwiseMax(box.high()));
}

//..................................................................................................
template <typename T>
BoundingBox<T> BoundingBox<T>::intersection(BoundingBox const& box) const {
    return BoundingBox(low_.cwiseMax(box.low()), high_.cwiseMin(box.high()));
}

//..................................................................................................
template <typename T>
BoundingBox<T> BoundingBox<T>::translation(T const& displacement) const {
    return BoundingBox(low_ + displacement, high_ + displacement);
}

//..................................................................................................
template <typename T>
BoundingBox<T> BoundingBox<T>::offset(typename T::Scalar distance) const {
    // Note: This only works for fixed size vectors. May need to update later.
    return BoundingBox(low_ - distance * T::Ones(), high_ + distance * T::Ones());
}

//..................................................................................................
template <typename T>
BoundingBox<T> BoundingBox<T>::scaled(typename T::Scalar factor) const {
    T const bb_center = center();
    T const half_diagonal = factor * (low_ - bb_center);
    return BoundingBox(bb_center + half_diagonal, bb_center - half_diagonal);
}

//..................................................................................................
template <typename T>
BoundingBox<T>& BoundingBox<T>::expand(T const& x) {
    low_ = low_.cwiseMin(x);
    high_ = high_.cwiseMax(x);
    return *this;
}

//..................................................................................................
template <typename T>
BoundingBox<T>& BoundingBox<T>::expand(BoundingBox const& box) {
    low_ = low_.cwiseMin(box.low());
    high_ = high_.cwiseMax(box.high());
    return *this;
}

//..................................................................................................
template <typename T>
BoundingBox<T>& BoundingBox<T>::intersect(BoundingBox const& box) {
    low_ = low_.cwiseMax(box.low());
    high_ = high_.cwiseMin(box.high());
    return *this;
}

//..................................................................................................
template <typename T>
BoundingBox<T>& BoundingBox<T>::translate(T const& displacement) {
    low_ += displacement;
    high_ += displacement;
    return *this;
}

//..................................................................................................
template <typename T>
BoundingBox<T>& BoundingBox<T>::apply_offset(typename T::Scalar distance) {
    // Note: This only works for fixed size vectors. May need to update later.
    low_ -= distance * T::Ones();
    high_ += distance * T::Ones();
    return *this;
}

//..................................................................................................
template <typename T>
BoundingBox<T>& BoundingBox<T>::scale(typename T::Scalar factor) {
    T const bb_center = center();
    T const half_diagonal = factor * (low_ - bb_center);
    low_ = bb_center + half_diagonal;
    high_ = bb_center - half_diagonal;
    return *this;
}

//--------------------------------------------------------------------------------------------------
template <typename T>
std::ostream& operator<<(std::ostream& os, BoundingBox<T> const& bb) {
    os << bb.low()[0] << ", " << bb.low()[1] << " to " << bb.high()[0] << ", " << bb.high()[1];
    return os;
}

#endif
