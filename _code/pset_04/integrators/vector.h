#ifndef VECTOR_H
#define VECTOR_H

#include <Eigen/Dense>

//--------------------------------------------------------------------------------------------------
using Scalar = double;
using Vector = Eigen::Matrix<Scalar, 2, 1>;

#endif
