#ifndef INTEGRATION_PATH_H
#define INTEGRATION_PATH_H

#include <iostream>
#include <tuple>
#include <vector>
#include "vector.h"

//--------------------------------------------------------------------------------------------------
template <typename Scalar, typename Vector>
struct IntegrationPath {
    std::vector<Scalar> times;
    std::vector<Vector> positions;

    void reserve(uint32_t n) {
        times.reserve(n);
        positions.reserve(n);
    }

    void push_back(Scalar t, Vector const& x) {
        times.push_back(t);
        positions.push_back(x);
    }

    void print_numpy(char const* name, Scalar min_delta_t) const {
        // Figure out which indices to print.
        std::vector<uint32_t> indices;
        indices.push_back(0);
        Scalar last_t = times.front();
        for (uint32_t i = 1; i < times.size(); ++i) {
            if (times[i] - last_t > min_delta_t) {
                indices.push_back(i);
                last_t = times[i];
            }
        }

        std::cout << name << "_times = np.array([" << times[indices.front()];
        for (uint32_t i = 0; i < indices.size(); ++i) {
            std::cout << ", " << times[indices[i]];
        }
        std::cout << "])\n";

        std::cout << name << "_positions = np.array([" << positions[indices.front()][0];
        for (uint32_t i = 0; i < indices.size(); ++i) {
            std::cout << ", " << positions[indices[i]][0];
        }
        std::cout << "])\n";
    }
};

//--------------------------------------------------------------------------------------------------
template <typename Integrator>
IntegrationPath<Scalar, Vector> integration_test(
    Integrator const& integrator,
    Vector const& x_initial,
    Scalar t_final,
    Scalar step_size
) {
    Vector x = x_initial;
    Scalar t = 0;

    IntegrationPath<Scalar, Vector> result;
    uint32_t const n_steps = static_cast<uint32_t>(t_final / step_size) + 1;
    result.reserve(n_steps);
    result.push_back(t, x);

    while (t < t_final) {
        t += step_size;
        x = integrator(x, step_size);
        result.push_back(t, x);
    }

    return result;
}

//--------------------------------------------------------------------------------------------------
template <typename Integrator>
IntegrationPath<Scalar, Vector> adaptive_integration_test(
    Integrator const& integrator,
    Vector const& x_initial,
    Scalar t_final,
    Scalar const tolerance
) {
    Vector x = x_initial;
    Scalar t = 0;
    Scalar step_size = 1e-3;

    IntegrationPath<Scalar, Vector> result;
    result.push_back(t, x);

    while (t < t_final) {
        t += step_size;
        std::tie(x, step_size) = integrator(x, step_size, tolerance);
        result.push_back(t, x);
    }

    return result;
}

//--------------------------------------------------------------------------------------------------
template <typename Integrator>
IntegrationPath<Scalar, Vector> adaptive_time_dep_integration_test(
    Integrator const& integrator,
    Scalar t_initial,
    Vector const& x_initial,
    Scalar t_final,
    Scalar const tolerance
) {
    Vector x = x_initial;
    Scalar t = t_initial;
    Scalar delta_t = 1e-3;

    IntegrationPath<Scalar, Vector> result;
    result.push_back(t, x);

    while (t < t_final) {
        t += delta_t;
        std::tie(x, delta_t) = integrator(t, x, delta_t, tolerance);
        result.push_back(t, x);
    }

    return result;
}

#endif
