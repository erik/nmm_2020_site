#include <iostream>
#include "integrators.h"
#include "integration_path.h"

//--------------------------------------------------------------------------------------------------
using Scalar = double;
using Vector = Eigen::Matrix<Scalar, 2, 1>;

//--------------------------------------------------------------------------------------------------
struct ChaoticPendulum {
    // original equation is l theta'' + (g + z'') sin theta = 0
    // x[0] is theta(t), x[1] is d theta/dt(t)
    // take z(t) = A cos(omega t), so z''(t) = -omega^2 A cos(omega t)
    // solving we get theta''(t) = -(g - omega^2 A cos(omega t)) * sin(theta) / l
    // bake a minus sign into g, and we have (g + omega^2 A cos(omega t)) * sin(theta) / l
    Vector operator()(Scalar t, Vector const& x) const {
        return Vector(
            x[1],
            (g_ + effective_amplitude_ * std::cos(omega_ * t)) * std::sin(x[0]) / length_
        );
    }

    static constexpr Scalar g_ = -9.8;
    Scalar length_;
    Scalar omega_;
    Scalar amplitude_;
    Scalar effective_amplitude_; // = omega_^2 * amplitude_

    void set_parameters(Scalar length, Scalar omega, Scalar amplitude) {
        length_ = length;
        omega_ = omega;
        amplitude_ = amplitude;
        effective_amplitude_ = omega * omega * amplitude;
    }
};

//--------------------------------------------------------------------------------------------------
int main() {
    ChaoticPendulum pendulum;

    auto const adaptive_rk4_integrator = [&](Scalar t, Vector const& x, Scalar step_size, Scalar tolerance) {
        return adaptive_rk4_step(pendulum, t, x, step_size, tolerance);
    };

    Vector const x_0 = Vector(0.1 * 3.1415926, 0);
    Scalar const t_0 = 0;
    Scalar const t_max = 100;
    Scalar const tolerance = 1e-8;

    // should be nearly sinusoidal periodic motion
    pendulum.set_parameters(1, 0, 0);
    auto path = adaptive_time_dep_integration_test(adaptive_rk4_integrator, t_0, x_0, t_max, tolerance);
    path.print_numpy("pendulum_01", 0.05);
    std::cout << "average step size: " << t_max / (path.times.size() - 1) << "\n\n";

    // adds an oscillating bias to the movement
    pendulum.set_parameters(1, 1 * 3.1415926, 0.25);
    path = adaptive_time_dep_integration_test(adaptive_rk4_integrator, t_0, x_0, t_max, tolerance);
    path.print_numpy("pendulum_02", 0.05);
    std::cout << "average step size: " << t_max / (path.times.size() - 1) << "\n\n";

    // finds a resonance and swings the pendulum around at a roughly constant rate
    pendulum.set_parameters(1, 1.2 * 3.1415926, 1.0);
    path = adaptive_time_dep_integration_test(adaptive_rk4_integrator, t_0, x_0, t_max, tolerance);
    path.print_numpy("pendulum_03", 0.05);
    std::cout << "average step size: " << t_max / (path.times.size() - 1) << "\n\n";

    // chaotic
    pendulum.set_parameters(1, 2 * 3.1415926, 1);
    path = adaptive_time_dep_integration_test(adaptive_rk4_integrator, t_0, x_0, t_max, tolerance);
    path.print_numpy("pendulum_04", 0.05);
    std::cout << "average step size: " << t_max / (path.times.size() - 1) << "\n\n";
}
