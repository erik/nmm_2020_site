#include "errors.h"

//--------------------------------------------------------------------------------------------------
Errors operator*(Scalar lhs, Errors const& rhs) {
    Errors result;
    result.value_error     = lhs * rhs.value_error;
    result.abs_value_error = lhs * rhs.abs_value_error;
    result.slope_error     = lhs * rhs.slope_error;
    result.abs_slope_error = lhs * rhs.abs_slope_error;
    result.mean_error      = lhs * rhs.mean_error;
    result.abs_mean_error  = lhs * rhs.abs_mean_error;
    result.l2_error        = lhs * rhs.l2_error;
    return result;
}

//--------------------------------------------------------------------------------------------------
void print_numpy(
    char const* name,
    std::vector<Scalar> const& step_sizes,
    std::vector<Errors> const& global_errors,
    std::vector<Errors> const& local_errors
){
    std::cout << name << " = {\n";
    std::cout << "    \"step_sizes\": ";
    print_numpy(step_sizes);
    std::cout << ",\n    \"g_value\": ";
    print_numpy(global_errors, [](Errors const& errors) { return errors.value_error; });
    std::cout << ",\n    \"g_abs_value\": ";
    print_numpy(global_errors, [](Errors const& errors) { return errors.abs_value_error; });
    std::cout << ",\n    \"g_slope\": ";
    print_numpy(global_errors, [](Errors const& errors) { return errors.slope_error; });
    std::cout << ",\n    \"g_abs_slope\": ";
    print_numpy(global_errors, [](Errors const& errors) { return errors.abs_slope_error; });
    std::cout << ",\n    \"g_mean\": ";
    print_numpy(global_errors, [](Errors const& errors) { return errors.mean_error; });
    std::cout << ",\n    \"g_abs_mean\": ";
    print_numpy(global_errors, [](Errors const& errors) { return errors.abs_mean_error; });
    std::cout << ",\n    \"g_l2\": ";
    print_numpy(global_errors, [](Errors const& errors) { return errors.l2_error; });
    std::cout << ",\n    \"l_value\": ";
    print_numpy(local_errors, [](Errors const& errors) { return errors.value_error; });
    std::cout << ",\n    \"l_abs_value\": ";
    print_numpy(local_errors, [](Errors const& errors) { return errors.abs_value_error; });
    std::cout << ",\n    \"l_slope\": ";
    print_numpy(local_errors, [](Errors const& errors) { return errors.slope_error; });
    std::cout << ",\n    \"l_abs_slope\": ";
    print_numpy(local_errors, [](Errors const& errors) { return errors.abs_slope_error; });
    std::cout << ",\n    \"l_mean\": ";
    print_numpy(local_errors, [](Errors const& errors) { return errors.mean_error; });
    std::cout << ",\n    \"l_abs_mean\": ";
    print_numpy(local_errors, [](Errors const& errors) { return errors.abs_mean_error; });
    std::cout << ",\n    \"l_l2\": ";
    print_numpy(local_errors, [](Errors const& errors) { return errors.l2_error; });
    std::cout << "\n}\n";
}
