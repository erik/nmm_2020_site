#ifndef INTEGRATORS_H
#define INTEGRATORS_H

#include <cmath>
#include <utility>

//--------------------------------------------------------------------------------------------------
// All these integrators are for numerically solving problems of the form  x'(t) = f(t, x). Note
// that x can be vector valued, so higher order problems can be solved by converting to a system of
// first order problems.

//--------------------------------------------------------------------------------------------------
// A single step of Euler integration
template <typename F, typename Vector, typename Scalar>
Vector euler_step(F const& f, Scalar t, Vector const& x, Scalar delta_t) {
    return x + delta_t * f(t, x);
}

//--------------------------------------------------------------------------------------------------
// A single step of fourth order Runge-Kutta
template <typename F, typename Vector, typename Scalar>
Vector rk4_step(F const& f, Scalar t, Vector const& x, Scalar delta_t) {
    Scalar const half_delta_t = static_cast<Scalar>(0.5) * delta_t;
    Vector result = x;
    // k1
    Vector tmp = delta_t * f(t, x);
    result += 0.16666666666666 * tmp;
    // k2
    tmp = delta_t * f(half_delta_t, x + static_cast<Scalar>(0.5) * tmp);
    result += 0.33333333333333 * tmp;
    // k3
    tmp = delta_t * f(half_delta_t, x + static_cast<Scalar>(0.5) * tmp);
    result += 0.33333333333333 * tmp;
    // k4
    tmp = delta_t * f(t + delta_t, x + tmp);
    result += 0.16666666666666 * tmp;
    return result;
}

//--------------------------------------------------------------------------------------------------
// A fourth order Runge-Kutta step that also returns a recommneded next step size, based on a local
// error tolerance.
template <typename F, typename Vector, typename Scalar>
std::pair<Vector, Scalar> adaptive_rk4_step(F const& f, Scalar t, Vector const& x, Scalar delta_t, Scalar tolerance) {
    // It's key that bigger * smaller != 1, so that it's possible to reach any step size.
    static constexpr Scalar bigger = 1.2;
    static constexpr Scalar smaller = 0.8;

    Scalar const half_delta_t = static_cast<Scalar>(0.5) * delta_t;
    Vector const full_step = rk4_step(f, t, x, delta_t);
    Vector const half1 = rk4_step(f, t, x, half_delta_t);
    Vector const half2 = rk4_step(f, half_delta_t, half1, half_delta_t);

    // TODO: I'm hard coding in an assumption that a Vector is only necessary because we're solving
    // a second (or higher) order system, and so the only error we really care about is the final
    // value. This should be more generic.
    Scalar const error_estimate = std::abs((full_step - half2)[0]);
    Scalar next_delta_t = delta_t;
    if (error_estimate > tolerance) {
        next_delta_t *= smaller;
    }
    else if (error_estimate < static_cast<Scalar>(0.5) * tolerance) {
        next_delta_t *= bigger;
    }

    return {half2, next_delta_t};
}

#endif
