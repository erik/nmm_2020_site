#ifndef ERRORS_H
#define ERRORS_H

#include <cmath>
#include <iostream>
#include <vector>
#include "vector.h"
#include "integration_path.h"

//--------------------------------------------------------------------------------------------------
struct Errors {
    Scalar value_error;
    Scalar abs_value_error;
    Scalar slope_error;
    Scalar abs_slope_error;
    Scalar mean_error;
    Scalar abs_mean_error;
    Scalar l2_error;

    Errors() { clear(); }

    void clear() {
        value_error     = 0;
        abs_value_error = 0;
        slope_error     = 0;
        abs_slope_error = 0;
        mean_error      = 0;
        abs_mean_error  = 0;
        l2_error        = 0;
    }

    static Errors evaluate(Vector const& computed, Vector const& expected) {
        Errors result;
        result.value_error     = computed[0] - expected[0];
        result.abs_value_error = std::abs(result.value_error);
        result.slope_error     = computed[1] - expected[1];
        result.abs_slope_error = std::abs(result.slope_error);
        result.mean_error      = 0.5 * result.value_error + 0.5 * result.slope_error;
        result.abs_mean_error  = std::abs(result.mean_error);
        result.l2_error        = (computed - expected).norm();
        return result;
    }

    Errors& operator+=(Errors const& rhs) {
        value_error     += rhs.value_error;
        abs_value_error += rhs.abs_value_error;
        slope_error     += rhs.slope_error;
        abs_slope_error += rhs.abs_slope_error;
        mean_error      += rhs.mean_error;
        abs_mean_error  += rhs.abs_mean_error;
        l2_error        += rhs.l2_error;
        return *this;
    }
};

//--------------------------------------------------------------------------------------------------
Errors operator*(Scalar lhs, Errors const& rhs);

//--------------------------------------------------------------------------------------------------
template <typename F>
Errors global_errors(IntegrationPath<Scalar, Vector> const& path, F const& oracle, Vector const& ic) {
    return Errors::evaluate(path.positions.back(), oracle(ic, path.times.back()));
}

//--------------------------------------------------------------------------------------------------
template <typename F>
Errors local_errors(IntegrationPath<Scalar, Vector> const& path, F const& oracle) {
    Errors errors;
    uint32_t const n_data_points = path.times.size();
    Scalar normalization = 1 / static_cast<Scalar>(n_data_points);
    for (uint32_t i = 1; i < n_data_points; ++i) {
        errors += normalization * Errors::evaluate(
            path.positions[i],
            oracle(path.positions[i - 1], path.times[i] - path.times[i - 1])
        );
    }
    return errors;
}

//--------------------------------------------------------------------------------------------------
// F is an accessor function
template <typename T, typename F>
void print_numpy(std::vector<T> const& vector, F const& f) {
    std::cout << "np.array([" << f(vector.front());
    for (uint32_t i = 1; i < vector.size(); ++i) {
        std::cout << ", " << f(vector[i]);
    }
    std::cout << "])";
}

//--------------------------------------------------------------------------------------------------
template <typename T>
void print_numpy(std::vector<T> const& vector) {
    print_numpy(vector, [](T const& t) { return t; });
}

//--------------------------------------------------------------------------------------------------
void print_numpy(
    char const* name,
    std::vector<Scalar> const& step_sizes,
    std::vector<Errors> const& global_errors,
    std::vector<Errors> const& local_errors
);

#endif
