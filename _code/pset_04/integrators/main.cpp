#include <iostream>
#include "errors.h"
#include "integrators.h"
#include "integration_path.h"

//--------------------------------------------------------------------------------------------------
struct HarmonicOscillator {
    // x[0] is x(t), x[1] is dx/dt(t)
    Vector operator()(Scalar, Vector const& x) const {
        return Vector(x[1], -x[0]);
    }
};

//--------------------------------------------------------------------------------------------------
int main() {
    HarmonicOscillator oscillator;

    auto const oscillator_oracle = [](Vector const& initial_conditions, Scalar t) {
        return Vector(
            initial_conditions[0] * std::cos(t) + initial_conditions[1] * std::sin(t),
            -initial_conditions[0] * std::sin(t) + initial_conditions[1] * std::cos(t)
        );
    };

    auto const euler_integrator = [&](Vector const& x, Scalar s) {
        return euler_step(oscillator, Scalar(0), x, s);
    };

    auto const rk4_integrator = [&](Vector const& x, Scalar s) {
        return rk4_step(oscillator, Scalar(0), x, s);
    };

    auto const adaptive_rk4_integrator = [&](Vector const& x, Scalar step_size, Scalar tolerance) {
        return adaptive_rk4_step(oscillator, Scalar(0), x, step_size, tolerance);
    };

    Vector const ic = Vector(1, 0);
    Scalar const t_max = 314.159265359;

    // Data for trajectory plots (Euler and RK4)
    {
        auto euler_results = integration_test(euler_integrator, ic, t_max, 0.01);
        auto rk4_results = integration_test(rk4_integrator, ic, t_max, 0.01);
        std::cout << "Euler steps: " << euler_results.times.size() << '\n';
        std::cout << "RK4 steps: " << rk4_results.times.size() << '\n';
        std::cout << '\n';
        // The next two lines print the data for the two trajectory plots.
        euler_results.print_numpy("euler_01", 0.1);
        rk4_results.print_numpy("rk4_01", 0.1);
        std::cout << '\n';
    }

    // Error analysis (Euler and RK4)
    {
        std::vector<Scalar> step_sizes = {1e-5, 1e-4, 1e-3, 1e-2, 1e-1};
        std::vector<Errors> euler_errors_global;
        std::vector<Errors> euler_errors_local;
        std::vector<Errors> rk4_errors_global;
        std::vector<Errors> rk4_errors_local;
        for (auto step_size : step_sizes) {
            auto euler_results = integration_test(euler_integrator, ic, t_max, step_size);
            euler_errors_global.push_back(global_errors(euler_results, oscillator_oracle, ic));
            euler_errors_local.push_back(local_errors(euler_results, oscillator_oracle));

            auto rk4_results = integration_test(rk4_integrator, ic, t_max, step_size);
            rk4_errors_global.push_back(global_errors(rk4_results, oscillator_oracle, ic));
            rk4_errors_local.push_back(local_errors(rk4_results, oscillator_oracle));
        }
        // These lines print the data for fixed step size error charts
        print_numpy("euler_errors", step_sizes, euler_errors_global, euler_errors_local);
        print_numpy("rk4_errors", step_sizes, rk4_errors_global, rk4_errors_local);
        std::cout << '\n';
    }

    // RK4 with adaptive stepping
    {
        std::vector<Scalar> tolerances = {1e-24, 1e-21, 1e-18, 1e-15, 1e-12, 1e-9, 1e-6, 1e-3};
        std::vector<Errors> ark4_errors_global;
        std::vector<Errors> ark4_errors_local;
        std::vector<Scalar> average_step_size;
        for (auto tolerance : tolerances) {
            auto ark4_results = adaptive_integration_test(adaptive_rk4_integrator, ic, t_max, tolerance);
            ark4_errors_global.push_back(global_errors(ark4_results, oscillator_oracle, ic));
            ark4_errors_local.push_back(local_errors(ark4_results, oscillator_oracle));
            average_step_size.push_back(ark4_results.times.back() / (ark4_results.times.size() - 1));
        }
        std::cout << "# step_sizes here is actually local tolerance level\n";
        print_numpy("ark4_errors", tolerances, ark4_errors_global, ark4_errors_local);
        std::cout << "ark4_step_sizes = ";
        print_numpy(average_step_size);
        std::cout << "\n\n";
    }
}
