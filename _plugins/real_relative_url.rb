require 'pathname'

# Jekyll's built-in "relative_url" filter does not generate relative URLs, it generates URLs that
# don't contain the domain. This plugin was posted to a GitHub issue in which @aioobe asks about
# making real relative URLs: https://github.com/jekyll/jekyll/issues/6360.

module Jekyll
  module RealRelativeUrl
    def real_relative_url(url)
      pageUrl = @context.registers[:page]["url"]
      pageDir = Pathname(pageUrl).parent
      Pathname(url).relative_path_from(pageDir).to_s
    end
  end
end

Liquid::Template.register_filter(Jekyll::RealRelativeUrl)
