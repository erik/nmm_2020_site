---
title: MAS.864
---

## The Nature of Mathematical Modeling

_Spring 2020_  
_Erik Strand_

Source code for this website lives [here](https://gitlab.cba.mit.edu/erik/nmm_2020_site).
