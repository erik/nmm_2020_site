---
title: Non-Euclidean Geometric Algebra
---

## Background

Recall that an [inner product space](https://en.wikipedia.org/wiki/Inner_product_space) is a vector
space equipped with a positive-definite [bilinear](https://en.wikipedia.org/wiki/Bilinear_form) or
[sesquilinear](https://en.wikipedia.org/wiki/Sesquilinear_form) form that maps pairs of vectors to
the underlying field (often $$\mathcal{R}$$ or $$\mathcal{C}$$). Specifically, the form $$(v, w)
\mapsto v \cdot w$$ usually must satisfy the following properties:

$$
v \cdot w = \overline{w \cdot v} \\
(av) \cdot w = a (v \cdot w) \\
(v + w) \cdot x = v \cdot x + w \cdot x \\
v \cdot v \geq 0 \\
v \cdot v = 0 \implies v = 0
$$

These properties allow the bilinear form to induce a metric, and are used to derive important
results in linear algebra like the [spectral
theorem](https://en.wikipedia.org/wiki/Spectral_theorem).

In this page, I explore what happens when we relax some of the requirements. By changing the
structure of the metric, new and interesting geometries appear. Most results and proofs are adapted
from Artin's classic [*Geometric Algebra*](https://en.wikipedia.org/wiki/Geometric_Algebra).

Throughout these notes, let $$V$$ be a vector space over a field $$F$$. The common thread will be
that this vector space is equipped with a bilinear form. This is a mapping $$(x, y) \mapsto x \cdot
y$$ from $$V \times V$$ to $$F$$ such that

$$
(av) \cdot w = a (v \cdot w) = v \cdot (aw) \\
(v + w) \cdot x = v \cdot x + w \cdot x \\
v \cdot (w + x) = v \cdot w + v \cdot x \\
$$

Two vectors $$v$$ and $$w$$ are called *orthogonal* if $$v \cdot w = 0$$.

## Non-Degenerate and Non-Singular Spaces

The *kernel* of a bilinear form is the subspace of $$V$$ that contains every vector $$v$$ that is
orthogonal to every $$w \in V$$ (including itself). The kernel always contains the zero vector. It's
commonly desired that this is the only vector in the kernel, so we have not one, but two special
names for this case: *non-singular* and *non-degenerate*. Conversely, if the kernel is non-trivial,
we call the bilinear form and its vector space *singular* or *degenerate*.

Equivalently, a bilinear form is non-singular if

$$
v \cdot w = 0 \quad \forall w \in V \implies v = 0
$$

Consider the natural map from $$V$$ (the vector space) to $$V^* $$ (the
[dual](https://en.wikipedia.org/wiki/Dual_space) of the vector space) given by $$v \mapsto v^* $$
where

$$
v^* = (x \mapsto v \cdot x)
$$

Non-singularity ensures that this map is
[injective](https://en.wikipedia.org/wiki/Injective_function):

$$
x^* = y^* \\
x \cdot v = y \cdot v \quad \forall v \in V \\
(x - y) \cdot v = 0 \quad \forall v \in V \\
x = y
$$

and hence a [bijection](https://en.wikipedia.org/wiki/Bijection) (since it is an injective map
between two vector spaces of the same dimension). This map is also linear, and preserves the
structure of the bilinear form, assuming we use the naturally induced bilinear form

$$
v^* \cdot w^* = v \cdot w
$$

on $$V^* $$. Hence the map is an [isomorphism](https://en.wikipedia.org/wiki/Isomorphism).

If $$V$$ is singular, then there is some nonzero vector $$v$$ such that $$v \cdot w = 0$$ for all
$$w$$ in $$V$$. As a special case, $$v \cdot v = 0$$. Thus singularity implies the existence of at
least one nonzero vector that squares to zero. Such a vector is called *isotropic*. Viewed the other
way around, if there are no isotropic vectors, the form cannot be singular. Thus standard inner
product spaces are non-singular, since positive-definiteness forbids isotropic vectors. But as we
will see, it is possible to have non-singular spaces with isotropic vectors, including non-singular
spaces in which all vectors are isotropic.

## Inducing Quadratic Forms and Symmetries

Let's assume we have a vector space $$V$$ (over a field $$F$$) with a bilinear form. For now we
assume nothing else (in particular, the form may be singular). We can still do some interesting
things. Define a map $$Q: V \rightarrow F$$ by

$$
Q(x) = x \cdot x
$$

Note that

$$
Q(ax) = (ax) \cdot (ax) = a^2 (x \cdot x) = a^2 Q(x)
$$

for any $$x \in V$$ and $$a \in F$$. A vector $$x$$ is isotropic if and only if $$Q(x) = 0$$.
Additionally, if the [characteristic](https://en.wikipedia.org/wiki/Characteristic_(algebra)) of
$$F$$ is not 2, then we can define a map $$B: V \times V \rightarrow F$$ by

$$
\begin{aligned}
B(x, y)
&= \frac{1}{2} Q(x + y) - \frac{1}{2} Q(x) - \frac{1}{2} Q(y) \\
&= \frac{1}{2} (x + y) \cdot (x + y) - \frac{1}{2} x \cdot x - \frac{1}{2} y \cdot y \\
&= \frac{1}{2} (x \cdot x + x \cdot y + y \cdot x + y \cdot y) - \frac{1}{2} x \cdot x - \frac{1}{2} y \cdot y \\
&= \frac{1}{2} (x \cdot y + y \cdot x)
\end{aligned}
$$

Since it's a linear combination of bilinear terms, it's bilinear. And because addition is
commutative, it's also *symmetric* (i.e. $$B(x, y) = B(y, x)$$). Even so, it induces $$Q$$ just
like our original non-symmetric form:

$$
B(x, x) = \frac{1}{2} (x \cdot x + x \cdot x) = x \cdot x = Q(x)
$$

It turns out that $$B$$ is the unique symmetric bilinear form that induces $$Q$$. To prove this,
assume we have another symmetric bilinear form $$B'$$ that induces $$Q$$ (i.e. $$B'(x, x) = Q(x)$$).
Then for any $$x, y \in V$$,

$$
B'(x + y, x + y) = B'(x, x) + 2 B'(x, y) + B'(y, y) \\
\begin{aligned}
2 B'(x, y)
&= B'(x + y, x + y) - B'(x, x) - B'(y, y) \\
&= Q(x + y) - Q(x) - Q(y) \\
&= 2B(x, y)
\end{aligned}
$$

So $$B' = B$$.

To summarize, define a quadratic form on $$V$$ to be a map $$Q:V \rightarrow F$$ such that $$Q(ax) =
a^2 Q(x)$$ and $$Q(x + y) - Q(x) - Q(y)$$ is bilinear. We have shown that each quadratic form
induces a symmetric bilinear form, and vice versa. Furthermore, we proved that each quadratic form
is induced by exactly one symmetric bilinear form. It's clear that every quadratic form is induced
by some symmetric bilinear form (this is why we require $$Q(x + y) - Q(x) - Q(y)$$ to be bilinear in
the definition of a quadratic form). So we can conclude that quadratic forms and symmetric bilinear
forms are in bijective correspondence.

By analogy to the induced symmetric bilinear form, we may consider the induced *antisymmetric*, or
*alternating* bilinear form (again assuming $$F$$ does not have characteristic 2).

$$
B_a(x, y) = \frac{1}{2} (x \cdot y - y \cdot x)
$$

This map has the property that $$B_a(x, y) = -B_a(y, x)$$ for any $$x$$ and $$y$$ (i.e. it is
antisymmetric). Additionally, $$B_a(x, x) = 0$$ for all $$x$$, and $$B_a(x, y) = 0$$ if $$x$$ and
$$y$$ are linearly dependent (i.e. it is
[alternating](https://en.wikipedia.org/wiki/Alternating_multilinear_map)). These properties are
equivalent as long as $$F$$ doesn't have characteristic 2.

Finally, note that

$$
\begin{aligned}
B(x, y) + B_a(x, y)
&= \frac{1}{2}(x \cdot y + y \cdot x + x \cdot y - y \cdot x) \\
&= x \cdot y
\end{aligned}
$$

recovers the original bilinear form.

## Symplectic and Orthogonal Spaces

Now let's assume that the form is bilinear, and that

$$
v \cdot w = 0 \implies w \cdot v = 0
$$

(It may be singular.) This is a relaxed form of symmetry. Because of these properties (and the
commutativity of multiplication in the underlying field $$F$$), for any $$v, w, x \in V$$,

$$
\begin{aligned}
0
&= (v \cdot w)(v \cdot x) - (v \cdot x)(v \cdot w) \\
&= v \cdot ((v \cdot w) x - (v \cdot x) w) \\
&= ((v \cdot w) x - (v \cdot x) w) \cdot v \\
&= (v \cdot w) (x \cdot v) - (v \cdot x) (w \cdot v)
\end{aligned} \\
$$

Thus

$$
(v \cdot w) (x \cdot v) = (v \cdot x) (w \cdot v)
$$

Or by setting $$w = v$$,

$$
(v \cdot v) (x \cdot v) = (v \cdot v) (v \cdot x)
$$

So if $$v \cdot v \neq 0$$, then $$v$$ commutes with all other vectors $$x$$. Flipping this around,
if two vectors $$v$$ and $$w$$ do not commute (i.e. $$v \cdot w \neq w \cdot v$$), then both $$v$$
and $$w$$ must square to zero.

$$
v \cdot w \neq w \cdot v \implies v \cdot v = w \cdot w = 0
$$

We can make this statement even stronger. Assume that there are two vectors $$v$$ and $$w$$ such
that $$v \cdot w \neq w \cdot v$$. Let $$x$$ be any other vector in $$V$$. We will demonstrate that
$$x \cdot x = 0$$. If $$x$$ doesn't commute with either $$v$$ or $$w$$, then we're done. So we may
assume $$v \cdot x = x \cdot v$$ and $$w \cdot x = x \cdot w$$. Using the relation we found earlier,

$$
\begin{aligned}
(v \cdot w) (x \cdot v)
&= (v \cdot x) (w \cdot v) \\
&= (w \cdot v) (x \cdot v) \\
\end{aligned}
$$

Since $$v \cdot w \neq w \cdot v$$, this can only be true if

$$
v \cdot x = x \cdot v = 0
$$

Similarly (just switch $$v$$ and $$w$$), we can show that

$$
w \cdot x = x \cdot w = 0
$$

But then

$$
\begin{aligned}
(v + x) \cdot w
&= v \cdot w \\
&\neq w \cdot v \\
&= w \cdot (v + x)
\end{aligned}
$$

This implies that

$$
\begin{aligned}
0
&= (v + x) \cdot (v + x) \\
&= v \cdot v + v \cdot x + x \cdot v + x \cdot x \\
&= x \cdot x
\end{aligned}
$$

This shows that there are two different cases for a vector space $$V$$ that has a bilinear form with
relaxed symmetry.

### Case 1: Symplectic Geometry

Here not all vectors commute. From the  work above it follows that $$v \cdot v = 0$$ for all $$v$$
in $$V$$ (this is the defining feature of symplectic forms). In particular, for any $$v, w$$ in
$$V$$,

$$
\begin{aligned}
0
&= (v + w) \cdot (v + w) \\
&= v \cdot v + v \cdot w + w \cdot v + w \cdot w \\
&= v \cdot w + w \cdot v
\end{aligned}
$$

So

$$
v \cdot w = -w \cdot v
$$

Thus $$v$$ and $$w$$ commute if and only if $$v \cdot w = 0$$. This guarantees the relaxed symmetry
condition that we started with.

### Case 2: Orthogonal Geometry

Here all vectors commute. That is, we have not just relaxed symmetry but a fully symmetric bilinear
form. As we saw earlier this also means that $$V$$ has a quadratic form that induces the symmetric
bilinear form (assuming $$F$$ doesn't have characteristic 2). Standard inner product spaces have
orthogonal geometry.

It can be proven that every orthogonal vector space can be decomposed into a [direct
sum](https://en.wikipedia.org/wiki/Direct_sum) of one dimensional subspaces, each of which is
orthogonal to all others (specifically, all vectors in one subspace are orthogonal to all vectors in
every other subspace).

Note that by these definitions both symplectic and orthogonal forms can be singular or non-singular;
we haven't imposed any such restriction here. Usually we will discuss non-singular symplectic and
orthogonal spaces; other authors put this in the definition.

It is possible to have forms with both orthogonal and symplectic properties. Specifically, forms
under which all vectors commute and square to zero. To be consistent this requires that $$v \cdot w
= w \cdot v$$ and $$v \cdot w = -w \cdot v$$, which can be true as long as 1) $$v \cdot w = 0$$ for
all vectors $$v$$ and $$w$$ (such vector spaces are called isotropic, and are necessarily singular
if they are not trivial), or 2) nonzero scalars are their own additive inverses (thus permitting $$v
\cdot w = -v \cdot w \neq 0$$) as occurs when $$F$$ has characteristic 2. From here on out we will
assume that the field $$F$$ has characteristic other than two, so that we don't need to consider the
latter case. Given this, the considerations of 1) suffice to show that if all vectors in an
orthogonal space square to zero, then the vector space is isotropic.

## The Hyperbolic Plane

A particularly important example of orthogonal or symplectic geometry is the *hyperbolic plane*,
defined as a non-singular vector space of dimension 2 with at least one isotropic vector.

Let $$V$$ be a hyperbolic plane, and $$v$$ an isotropic vector in $$V$$. We will find another
isotropic vector $$w$$ such that $$v \cdot w = 1$$. Let $$x$$ be any nonzero vector not in the span
of $$v$$. Then $$v$$ and $$x$$ span $$V$$, so for some scalars $$a$$ and $$b$$,

$$
w = a v + b x
$$

Then since $$v$$ is isotropic,

$$
v \cdot w = v \cdot (av + bx) = b (v \cdot x)
$$

Note that $$v \cdot x$$ cannot be zero, as otherwise $$v$$ would be in the kernel of $$V$$ and $$V$$
would be singular. Therefore we can ensure that $$v \cdot w = 1$$ by setting

$$
b = (v \cdot x)^{-1}
$$

Now if $$V$$ is symplectic, $$w \cdot w$$ has to be zero, so it doesn't matter what $$a$$ is. If
$$V$$ is orthogonal, we solve (recall all vectors in an orthogonal space commute)

$$
\begin{aligned}
0 &= w \cdot w \\
&= 2 (ab) (v \cdot x) + b^2 (x \cdot x) \\
&= 2 a + \frac{(x \cdot x)}{(v \cdot x)^2} \\
\end{aligned} \\
a = \frac{-(x \cdot x)}{2 (v \cdot x)^2}
$$

Thus whether $$V$$ is symplectic or orthogonal, we now have vectors $$v$$ and $$w$$ that span $$V$$
and such that

$$
v \cdot v = w \cdot w = 0 \\
v \cdot w = 1
$$

If $$V$$ is orthogonal, then scalar multiples of $$v$$ and $$w$$ are the only isotropic vectors.

The canonical orthogonal hyperbolic plane is constructed as a two dimensional vector space over
$$\mathcal{R}$$ with bilinear form $$(a, b) \cdot (c, d) = ad + bc$$. The associated quadratic form
$$Q$$ maps $$(a, b)$$ to $$2ab$$. So the canonical basis vectors $$(1, 0)$$ and $$(0, 1)$$ generate
the two isotropic subspaces. The space is non-singular. Curves of constant $$Q$$ are hyperbolas.

The canonical symplectic hyperbolic plane is construced as a two dimensional vector space over
$$\mathcal{R}$$ with bilinear form $$(a, b) \cdot (c, d) = ad - bc$$. The associated quadratic form
maps all vectors to zero, as required in a symplectic space. That is, all vectors are isotropic.
However the space is still non-singular (for any fixed $$a$$ and $$b$$, you can choose $$c$$ and
$$d$$ so $$ad - bc \neq 0$$). Note that this space is isomorphic to the complex plane as a vector
space over $$\mathcal{R}$$, with bilinear form

$$
\begin{aligned}
(a + bi) \cdot (c + di)
&= \langle i(a + bi), c + di \rangle \\
&= \langle -b + ia, c + di \rangle \\
&= ad - bc
\end{aligned}
$$

where $$\langle x, y \rangle$$ is the standard Euclidean norm of $$\mathcal{R}^2$$. This is why such
spaces are called symplectic (which is a [calque](https://en.wikipedia.org/wiki/Calque) that
exchanges the Latin roots of "complex" for Greek ones).

It can be proven that every non-singular symplectic space is an orthogonal sum of hyperbolic planes
(i.e. a direct sum of subspaces, each of which is a hyperbolic plane, and each of which is
orthogonal to all others).
