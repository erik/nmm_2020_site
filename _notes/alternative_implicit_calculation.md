---
title: An Alternative Numeric Scheme for Implicit Diffusion
---

To implement the standard implicit scheme for fixed boundary conditions, we need to solve a system
of linear equations that looks like this (here written for only five samples in $$x$$, but the
structure should be clear).

$$
\begin{bmatrix}
1       & 0           & 0           & 0           & 0       \\
-\alpha & 2\alpha + 1 & -\alpha     & 0           & 0       \\
0       & -\alpha     & 2\alpha + 1 & -\alpha     & 0       \\
0       & 0           & -\alpha     & 2\alpha + 1 & -\alpha \\
0       & 0           & 0           & 0           & 1
\end{bmatrix}
\begin{bmatrix}
u_1^{n+1} \\
u_2^{n+1} \\
u_3^{n+1} \\
u_4^{n+1} \\
u_5^{n+1}
\end{bmatrix}

=
\begin{bmatrix}
u_1^n \\
u_2^n \\
u_3^n \\
u_4^n \\
u_5^n
\end{bmatrix}
$$

This is a specific case of the general tridiagonal system.

$$
\begin{bmatrix}
b_1 & c_1 & 0   & 0   & 0   \\
a_2 & b_2 & c_2 & 0   & 0   \\
0   & a_3 & b_3 & c_3 & 0   \\
0   & 0   & a_4 & b_4 & c_4 \\
0   & 0   & 0   & a_5 & b_5
\end{bmatrix}
\begin{bmatrix} x_1 \\ x_2 \\ x_3 \\ x_4 \\ x_5 \end{bmatrix}
=
\begin{bmatrix}
y_1 \\
y_2 \\
y_3 \\
y_4 \\
y_5
\end{bmatrix}
$$

We can use Gaussian elimination to systematically remove all off-diagonal elements. First, to get
rid of $$a_2$$, multiply the first row by $$a_2/b_1$$ and subtract it from the second.

$$
\begin{bmatrix}
b_1 & c_1                      & 0   & 0   & 0   \\
0   & b_2 - \frac{a_2}{b_1}c_1 & c_2 & 0   & 0   \\
0   & a_3                      & b_3 & c_3 & 0   \\
0   & 0                        & a_4 & b_4 & c_4 \\
0   & 0                        & 0   & a_5 & b_5
\end{bmatrix}
\begin{bmatrix} x_1 \\ x_2 \\ x_3 \\ x_4 \\ x_5 \end{bmatrix}
=
\begin{bmatrix}
y_1 \\
y_2 - \frac{a_2}{b_1} y_1 \\
y_3 \\
y_4 \\
y_5
\end{bmatrix}
$$

We can write this as

$$
\begin{bmatrix}
b_1 & c_1  & 0   & 0   & 0   \\
0   & b_2' & c_2 & 0   & 0   \\
0   & a_3  & b_3 & c_3 & 0   \\
0   & 0    & a_4 & b_4 & c_4 \\
0   & 0    & 0   & a_5 & b_5
\end{bmatrix}
\begin{bmatrix} x_1 \\ x_2 \\ x_3 \\ x_4 \\ x_5 \end{bmatrix}
=
\begin{bmatrix}
y_1 \\
y_2' \\
y_3 \\
y_4 \\
y_5
\end{bmatrix}
$$

where $$b_2' = b_2 - c_1 a_2 / b_1$$ and $$y_2' = y2 - y_1 a_2 / b_1$$.

To knock out $$a_3$$, we multiply the second row by $$a_3 / b_2'$$ and subtract from the third.
Thus $$b_3' = b_3 - c_2 a_3 / b_2'$$, and $$y_3' = y_3 - y_2' a_3 / b_2'$$. After repeating this
process for all remaining rows, we end up with

$$
\begin{bmatrix}
b_1' & c_1  & 0    & 0    & 0    \\
0    & b_2' & c_2  & 0    & 0    \\
0    & 0    & b_3' & c_3  & 0    \\
0    & 0    & 0    & b_4' & c_4  \\
0    & 0    & 0    & 0    & b_5'
\end{bmatrix}
\begin{bmatrix} x_1 \\ x_2 \\ x_3 \\ x_4 \\ x_5 \end{bmatrix}
=
\begin{bmatrix}
y_1' \\
y_2' \\
y_3' \\
y_4' \\
y_5'
\end{bmatrix}
$$

where

$$
\begin{aligned}
b_i' &=
\begin{cases}
b_1 &\text{for } i = 1 \\
b_i - \frac{a_i}{b_{i-1}'} c_{i-1} &\text{for } i > 1
\end{cases} \\

y_i' &=
\begin{cases}
y_1 &\text{for } i = 1 \\
y_i - \frac{a_i}{b_{i-1}'} y_{i-1}' &\text{for } i > 1
\end{cases} \\
\end{aligned}
$$

Now we remove the entries above the diagonal, starting with $$c_4$$. Multiply the last row by $$c_4
/ b_5'$$ and subtract from the fourth. This doesn't change $$b_4'$$, but it does yield $$y_4'' =
y_4' - y_5' c_4/ b_5'$$. So after doing this $$m - 1$$ times (matrices shown for $$m = 5$$), we have

$$
\begin{bmatrix}
b_1' & 0    & 0    & 0    & 0    \\
0    & b_2' & 0    & 0    & 0    \\
0    & 0    & b_3' & 0    & 0    \\
0    & 0    & 0    & b_4' & 0    \\
0    & 0    & 0    & 0    & b_5'
\end{bmatrix}
\begin{bmatrix} x_1 \\ x_2 \\ x_3 \\ x_4 \\ x_5 \end{bmatrix}
=
\begin{bmatrix}
y_1'' \\
y_2'' \\
y_3'' \\
y_4'' \\
y_5''
\end{bmatrix}
$$

where

$$
y_i'' =
\begin{cases}
y_m'                                 &\text{for } i = m \\
y_i' - \frac{c_i}{b_{i+1}'} y_{i+1}' &\text{for } i < m
\end{cases} \\
$$

Now the solution is evident: $$x_i = y_i'' / b_i'$$.

For our particular system,

$$
\begin{aligned}
a_i &=
\begin{cases}
-\alpha &\text{for } i < m \\
0       &\text{for } i = m
\end{cases} \\

b_i &=
\begin{cases}
1            &\text{for } i = 1 \\
2 \alpha + 1 &\text{for } 1 < i < m \\
1            &\text{for } i = m
\end{cases} \\

c_i &=
\begin{cases}
0       &\text{for } i = 1 \\
-\alpha &\text{for } i > 1
\end{cases} \\

x_i &= u_i^{n + 1} \\
y_i &= u_i^n
\end{aligned}
$$

where

$$
\alpha = \frac{D \Delta t}{(\Delta x)^2}
$$

Plugging things in, we find that

$$
\begin{aligned}
b_i' &=
\begin{cases}
1            &\text{for } i = 1 \\
2 \alpha + 1 &\text{for } i = 2 \\
\frac{(\alpha + 1)(3 \alpha + 1)}{2 \alpha + 1} &\text{for } 2 < i < m \\
1            &\text{for } i = m
\end{cases} \\

y_i' &=
\begin{cases}
u_1^n                &\text{for } i = 1 \\
u_2^n + \alpha u_1^n &\text{for } i = 2 \\
u_3^n + \frac{\alpha}{2 \alpha + 1} y_2' &\text{for } i = 3 \\
u_i^n + \frac{\alpha (2 \alpha + 1)}{(\alpha + 1)(3 \alpha + 1)} y_{i-1}' &\text{for } 3 < i < m \\
u_m^n &\text{for } i = m
\end{cases} \\

y_i'' &=
\begin{cases}
u_m^n                    &\text{for } i = m \\
y_{m-1}' + \alpha u_m^n  &\text{for } i = m - 1 \\
y_i' + \frac{\alpha (2 \alpha + 1)}{(\alpha + 1)(3 \alpha + 1)} y_{i+1}' &\text{for } 1 < i < m - 1 \\
u_1^n                    &\text{for } i = 1
\end{cases} \\
\end{aligned}
$$
