---
title: Homogeneous Linear ODEs
---

How do we know the solutions of an $$n$$th order linear homogeneous ODE form a vector space of
dimension $$n$$? Is this true only in the complex case, or for real valued solutions as well? This
page answers these questions.

## Summary

The short answer is that the initial conditions that specify a unique solution to an $$n$$th order
linear homogeneous ODE are an $$n$$ dimensional vector. (That is, $$x(0)$$, $$\dot{x}(0)$$,
$$\dots$$, $$x^{(n - 1)}$$.) So we can find $$n$$ linearly independent sets of initial conditions.
Then each one of these evolves differently, so the resulting $$n$$ functions are linearly
independent. Doesn't matter if your functions are real or complex.

## Partial Proof

Let's formalize this a bit. Say we have a $$n$$th order linear homogeneous ODE.

$$
\sum_{i = 0}^{n - 1} A_i(t) x^{(n)}(t) + x^{(n)}(t) = 0
$$

Assume that all $$A_i$$ are continuous. It's possible to write this as a coupled system of $$n$$
first order ODEs. First, define $$n$$ functions $$x_0, \dots, x_{n - 1}$$. Then the following matrix
system describes the same problem.

$$
\begin{bmatrix}
\dot{x}_0(t) \\
\dot{x}_1(t) \\
\dot{x}_3(t) \\
\vdots \\
\dot{x}_{n - 1}(t)
\end{bmatrix}

=

\begin{bmatrix}
0 & 1 & 0 & \dots & 0 \\
0 & 0 & 1 & \dots & 0 \\
\vdots & \vdots & \vdots & \ddots & \vdots \\
0 & 0 & 0 & \dots & 1 \\
-A_0(t) & -A_1(t) & -A_2(t) & \dots & -A_{n - 1}(t)
\end{bmatrix}

\begin{bmatrix}
x_0(t) \\
x_1(t) \\
x_2(t) \\
\vdots \\
x_{n - 1}(t)
\end{bmatrix}
$$

Basically we're saying that each function is the derivative of the one before it, i.e. $$\dot{x}_1 =
x_2$$, so really $$x_i = x^{(i)}$$. Then the last row of the matrix defines $$\dot{x}_{n - 1}$$
(really $$x^{(n)}$$) in terms of the others.

Let's call that matrix $$\mathbb{A}(t)$$. Now solving the original ODE amounts to solving

$$
\frac{d}{dt} \overrightharpoon{x}(t) = \mathbb{A}(t) \overrightharpoon{x}(t)
$$

Or, if we find multiple solutions, we can use them as columns to build a matrix $$\mathbb{X}(t)$$.

$$
\frac{d}{dt} \mathbb{X}(t) = \mathbb{A}(t) \mathbb{X}(t)
$$

Now the columns of $$\mathbb{X}(0)$$ are just different sets of initial conditions; we can choose
them arbitrarily (gonna state this without proof, sorry) and then integrate forward to solve for the
rest of time. Let's choose them so they're linearly independent. This means that the determinant of
$$\mathbb{X}(0)$$ is nonzero.

What is the determinant of $$\mathbb{X}$$ at some future time $$t$$? This is answered by Liouville's
Formula.

$$
\det[\mathbb{X}(t)] = \det[\mathbb{X}(0)] \exp \left( \int_0^t \operatorname{tr}[\mathbb{A}(s)] ds \right)
$$

Crucially, as long as the value of the integral isn't $$-\infty$$ (or in the complex case, doesn't
have real part $$-\infty$$), then the factor by which we multiply the original determinant will not
be zero. Thus $$\det[\mathbb{X}(t)]$$ is nonzero for all $$t$$. Thus the columns (and rows) of
$$\mathbb{X}(t)$$ are always linearly independent.

This tells us what we want to know, and more. In particular, at no single point in time do any of
the solutions have linearly dependent values and derivatives. This is why paths in phase space can
never cross. This also means the functions can't be linearly dependent, since for their values to
be related by a scalar factor (over some open interval), their derivatives would have to as well.

## Mapping Between Complex and Real Solutions

Now assume all $$A_i(t)$$ and $$f(t)$$ are real valued. Let $$y(t) = r(t) + i c(t)$$ be any
solution. Then

$$
\begin{aligned}
f(t)
&= \sum_{i = 0}^n A_i(t) y^{(n)}(t) \\
&= \sum_{i = 0}^n A_i(t) \left( r^{(n)}(t) + i c^{(n)}(t) \right) \\
&= \sum_{i = 0}^n A_i(t) r^{(n)}(t) + i \sum_{i = 0}^n A_i(t) c^{(n)}(t)
\end{aligned}
$$

But since $$f$$ is real, this means

$$
\sum_{i = 0}^n A_i(t) c^{(n)}(t) = 0
$$

In other words, the imaginary component is a solution of the associated homogeneous equation.

As a result, $$r(t)$$ is a solution of the original equation on its own.

$$
f(t) = \sum_{i = 0}^n A_i(t) r^{(n)}(t)
$$

This proves that the real projection operator preserves solutions. (As a corollary, this also
demonstrates the existence of real solutions.) Note that this is true despite the fact that the
real projection operator is nonlinear and nonholomorphic.

Conversely, any real valued solution is of course also a complex solution (it's a complex solution
that happens to have zero imaginary part). Furthermore, given any real valued solution $$r(t)$$ of
the (potentially) inhomogeneous equation, and any real valued solution $$c(t)$$ of the associated
homogeneous equation, then by linearity $$y(t) = r(t) + i c(t)$$ is also a solution.

## Example

Let's look at the harmonic oscillator as an example.

$$
\ddot{x} + \omega^2 x = 0
$$

It's easiest to solve it using complex numbers. The general solution is

$$
\begin{aligned}
x(t)
&= A e^{i \omega t} + B e^{-i \omega t} \\
&= A (\cos(\omega t) + i \sin(\omega t)) + B(\cos(\omega t) - i \sin(\omega t)) \\
&= (A + B) \cos(\omega t) + i (A - B) \sin(\omega t)
\end{aligned}
$$

If we take $$A$$ and $$B$$ to be real numbers, the last line shows the real and imaginary parts of
the solution. And since the problem is homogeneous, this means that both $$\cos(\omega t)$$ and
$$\sin(\omega t)$$ are also solutions. They are linearly independent, so they form a basis for all
the real solutions.

Sometimes it can be helpful to see how a particular complex solution maps to a particular real one.
So let's let $$A = A_R + i A_I$$ and $$B = B_R + i B_I$$ be complex numbers. Then the general
solution's real and imaginary parts are

$$
\begin{aligned}
\operatorname{Re}[x](t) &= (A_R + B_R) \cos(\omega t) - (A_I - B_I) \sin(\omega t) \\
\operatorname{Im}[x](t) &= (A_R - B_R) \sin(\omega t) + (A_I + B_I) \cos(\omega t)
\end{aligned}
$$

If instead we write the coefficients as $$A = |A| e^{i \omega \phi_A}$$ and $$B = |B| e^{i \omega
\phi_B}$$, the general solution's real and imaginary parts are

$$
\begin{aligned}
\operatorname{Re}[x](t) &= |A| \cos(\omega (t + \phi_A)) + |B| \cos(\omega (t - \phi_B)) \\
\operatorname{Im}[x](t) &= |A| \sin(\omega (t + \phi_A)) - |B| \sin(\omega (t - \phi_B))
\end{aligned}
$$
