---
title: Final Project&#58; Differentiable Simulation
---

# Final Project: Differentiable Simulation

The main focus of my final project will be to solve inverse dynamics problems using differentiable
physics simulations.

- [basic theory](./notes/differentiable_simulation.html)
- [gradient estimators](./notes/optimization_of_expected_value.html)
- [background for symplectic integration](./notes/orthogonal_and_symplectic_geometry.html)

## Motivation

We've seen how under the hood, most simulations are ODE or PDE integration problems that are
discretized and solved numerically. The integrators (like Euler, RK4, or finite differences rules)
at the end of the day tend to consist of compositions of simple linear combinations of function
evaluations, so there's no reason we can't differentiate through a whole simulation by repeatedly
applying the chain rule. This tells us how the results of the simulation (whether that's the final
configuration of matter, or an integral that was computed over the duration of the simulation)
change with respect to all the initial conditions (like initial displacements and momenta) and/or
the parameters of the simulation itself (like step size, or constants of physical laws). This
enables us to use gradient descent to tune these parameters to achieve desired goals.

I've written out the basic theory of differentiable ODE simulation
[here](./notes/differentiable_simulation.html).

A related technique, borrowed from reinforcement learning, tries to get around the need for
calculating derivatives by using a statistical estimator of the gradient instead. This is sometimes
necessary, for instance if the initial conditions of the problem are produced by a non-differential
function of the parameters you want to optimize. I've developed notes on this technique
[here](./notes/optimization_of_expected_value.html).

Finally, as a piece of important background knowledge, I'm also interested in [geometric
integration](https://en.wikipedia.org/wiki/Geometric_integrator), specifically [symplectic
integration](https://en.wikipedia.org/wiki/Symplectic_integrator). These techniques guarantee (up to
numeric precision) that physically conserved quantities (e.g. energy) are preserved by the
integrator. This prevents a wide range of issues inherent in general purpose integrators, such as
the amplification that is so easy to see with Euler Integration. The math involved is pretty
formidable, so I'm working towards a full understanding from the ground up. My notes on the subject
are [here](./notes/orthogonal_and_symplectic_geometry.html).
